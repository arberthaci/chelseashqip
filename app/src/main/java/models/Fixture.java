package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class Fixture {
    private SelfCompetitionHomeTeamAwayTeamLink _links;
    @SerializedName("date")
    private String date;
    @SerializedName("status")
    private String status;
    @SerializedName("matchday")
    private int matchday;
    @SerializedName("homeTeamName")
    private String homeTeamName;
    @SerializedName("awayTeamName")
    private String awayTeamName;
    private Result result;
    private Odds odds;

    public SelfCompetitionHomeTeamAwayTeamLink get_links() {
        return _links;
    }

    public void set_links(SelfCompetitionHomeTeamAwayTeamLink _links) {
        this._links = _links;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMatchday() {
        return matchday;
    }

    public void setMatchday(int matchday) {
        this.matchday = matchday;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Odds getOdds() {
        return odds;
    }

    public void setOdds(Odds odds) {
        this.odds = odds;
    }
}
