package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 16-03-17.
 */
public class League {
    private SelfTeamsFixturesLeagueTableLink _links;
    @SerializedName("id")
    private long id;
    @SerializedName("caption")
    private String caption;
    @SerializedName("league")
    private String league;
    @SerializedName("year")
    private int year;
    @SerializedName("currentMatchday")
    private int currentMatchday;
    @SerializedName("numberOfMatchdays")
    private int numberOfMatchdays;
    @SerializedName("numberOfTeams")
    private int numberOfTeams;
    @SerializedName("numberOfGames")
    private int numberOfGames;
    @SerializedName("lastUpdated")
    private String lastUpdated;

    public SelfTeamsFixturesLeagueTableLink get_links() {
        return _links;
    }

    public void set_links(SelfTeamsFixturesLeagueTableLink _links) {
        this._links = _links;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getCurrentMatchday() {
        return currentMatchday;
    }

    public void setCurrentMatchday(int currentMatchday) {
        this.currentMatchday = currentMatchday;
    }

    public int getNumberOfMatchdays() {
        return numberOfMatchdays;
    }

    public void setNumberOfMatchdays(int numberOfMatchdays) {
        this.numberOfMatchdays = numberOfMatchdays;
    }

    public int getNumberOfTeams() {
        return numberOfTeams;
    }

    public void setNumberOfTeams(int numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
