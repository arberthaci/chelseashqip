package models;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class SelfCompetitionHomeTeamAwayTeamLink {
    private SelfLink self;
    private CompetitionLink competition;
    private TeamsLink homeTeam;
    private TeamsLink awayTeam;

    public SelfLink getSelf() {
        return self;
    }

    public void setSelf(SelfLink self) {
        this.self = self;
    }

    public CompetitionLink getCompetition() {
        return competition;
    }

    public void setCompetition(CompetitionLink competition) {
        this.competition = competition;
    }

    public TeamsLink getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(TeamsLink homeTeam) {
        this.homeTeam = homeTeam;
    }

    public TeamsLink getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(TeamsLink awayTeam) {
        this.awayTeam = awayTeam;
    }
}
