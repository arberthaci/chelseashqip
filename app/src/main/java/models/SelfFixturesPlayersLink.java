package models;

/**
 * Created by root on 16-03-17.
 */
public class SelfFixturesPlayersLink {
    private SelfLink self;
    private FixturesLink fixtures;
    private PlayersLink players;

    public SelfLink getSelf() {
        return self;
    }

    public void setSelf(SelfLink self) {
        this.self = self;
    }

    public FixturesLink getFixtures() {
        return fixtures;
    }

    public void setFixtures(FixturesLink fixtures) {
        this.fixtures = fixtures;
    }

    public PlayersLink getPlayers() {
        return players;
    }

    public void setPlayers(PlayersLink players) {
        this.players = players;
    }
}
