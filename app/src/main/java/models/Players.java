package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class Players {
    private SelfTeamLink _links;
    @SerializedName("count")
    private int count;
    private ArrayList<Player> players;

    public SelfTeamLink get_links() {
        return _links;
    }

    public void set_links(SelfTeamLink _links) {
        this._links = _links;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }
}
