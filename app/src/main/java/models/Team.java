package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 16-03-17.
 */
public class Team {
    private SelfFixturesPlayersLink _links;
    @SerializedName("name")
    private String name;
    @SerializedName("code")
    private String code;
    @SerializedName("shortName")
    private String shortName;
    @SerializedName("squadMarketValue")
    private String squadMarketValue;
    @SerializedName("crestUrl")
    private String crestUrl;

    public SelfFixturesPlayersLink get_links() {
        return _links;
    }

    public void set_links(SelfFixturesPlayersLink _links) {
        this._links = _links;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSquadMarketValue() {
        return squadMarketValue;
    }

    public void setSquadMarketValue(String squadMarketValue) {
        this.squadMarketValue = squadMarketValue;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public void setCrestUrl(String crestUrl) {
        this.crestUrl = crestUrl;
    }
}
