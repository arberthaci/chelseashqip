package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arber Thaci on 11/10/2016.
 */

public class Odds {
    @SerializedName("homeWin")
    private double homeWin;
    @SerializedName("draw")
    private double draw;
    @SerializedName("awayWin")
    private double awayWin;

    public double getHomeWin() {
        return homeWin;
    }

    public void setHomeWin(double homeWin) {
        this.homeWin = homeWin;
    }

    public double getDraw() {
        return draw;
    }

    public void setDraw(double draw) {
        this.draw = draw;
    }

    public double getAwayWin() {
        return awayWin;
    }

    public void setAwayWin(double awayWin) {
        this.awayWin = awayWin;
    }
}
