package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class Player {
    @SerializedName("name")
    private String name;
    @SerializedName("position")
    private String position;
    @SerializedName("jerseyNumber")
    private int jerseyNumber;
    @SerializedName("dateOfBirth")
    private String dateOfBirth;
    @SerializedName("nationality")
    private String nationality;
    @SerializedName("contractUntil")
    private String contractUntil;
    @SerializedName("marketValue")
    private String marketValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getContractUntil() {
        return contractUntil;
    }

    public void setContractUntil(String contractUntil) {
        this.contractUntil = contractUntil;
    }

    public String getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(String marketValue) {
        this.marketValue = marketValue;
    }
}
