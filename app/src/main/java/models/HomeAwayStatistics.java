package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class HomeAwayStatistics {
    @SerializedName("goals")
    private int goals;
    @SerializedName("goalsAgainst")
    private int goalsAgainst;
    @SerializedName("wins")
    private int wins;
    @SerializedName("draws")
    private int draws;
    @SerializedName("losses")
    private int losses;

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }
}
