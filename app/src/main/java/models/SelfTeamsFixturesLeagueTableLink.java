package models;

/**
 * Created by root on 16-03-17.
 */
public class SelfTeamsFixturesLeagueTableLink {
    private SelfLink self;
    private TeamsLink teams;
    private FixturesLink fixtures;
    private LeagueTableLink leagueTable;

    public SelfLink getSelf() {
        return self;
    }

    public void setSelf(SelfLink self) {
        this.self = self;
    }

    public TeamsLink getTeams() {
        return teams;
    }

    public void setTeams(TeamsLink teams) {
        this.teams = teams;
    }

    public FixturesLink getFixtures() {
        return fixtures;
    }

    public void setFixtures(FixturesLink fixtures) {
        this.fixtures = fixtures;
    }

    public LeagueTableLink getLeagueTable() {
        return leagueTable;
    }

    public void setLeagueTable(LeagueTableLink leagueTable) {
        this.leagueTable = leagueTable;
    }
}
