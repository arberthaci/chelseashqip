package models;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class SelfTeamLink {
    private SelfLink self;
    private TeamsLink team;

    public SelfLink getSelf() {
        return self;
    }

    public void setSelf(SelfLink self) {
        this.self = self;
    }

    public TeamsLink getTeam() {
        return team;
    }

    public void setTeam(TeamsLink team) {
        this.team = team;
    }
}
