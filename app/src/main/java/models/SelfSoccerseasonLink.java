package models;

/**
 * Created by root on 16-03-17.
 */
public class SelfSoccerseasonLink {
    private SelfLink self;
    private CompetitionLink soccerseason;

    public SelfLink getSelf() {
        return self;
    }

    public void setSelf(SelfLink self) {
        this.self = self;
    }

    public CompetitionLink getSoccerseason() {
        return soccerseason;
    }

    public void setSoccerseason(CompetitionLink soccerseason) {
        this.soccerseason = soccerseason;
    }
}
