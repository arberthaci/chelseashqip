package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class LeagueTable {
    private SelfSoccerseasonLink _links;
    @SerializedName("leagueCaption")
    private String leagueCaption;
    @SerializedName("matchday")
    private int matchday;
    private ArrayList<Standing> standing;

    public SelfSoccerseasonLink get_links() {
        return _links;
    }

    public void set_links(SelfSoccerseasonLink _links) {
        this._links = _links;
    }

    public String getLeagueCaption() {
        return leagueCaption;
    }

    public void setLeagueCaption(String leagueCaption) {
        this.leagueCaption = leagueCaption;
    }

    public int getMatchday() {
        return matchday;
    }

    public void setMatchday(int matchday) {
        this.matchday = matchday;
    }

    public ArrayList<Standing> getStanding() {
        return standing;
    }

    public void setStanding(ArrayList<Standing> standing) {
        this.standing = standing;
    }
}
