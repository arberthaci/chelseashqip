package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class Standing {
    private TeamsLink _links;
    @SerializedName("position")
    private int position;
    @SerializedName("teamName")
    private String teamName;
    @SerializedName("crestURI")
    private String crestURI;
    @SerializedName("playedGames")
    private int playedGames;
    @SerializedName("points")
    private int points;
    @SerializedName("goals")
    private int goals;
    @SerializedName("goalsAgainst")
    private int goalsAgainst;
    @SerializedName("goalDifference")
    private int goalDifference;
    @SerializedName("wins")
    private int wins;
    @SerializedName("draws")
    private int draws;
    @SerializedName("losses")
    private int losses;
    private HomeAwayStatistics home;
    private HomeAwayStatistics away;

    public TeamsLink get_links() {
        return _links;
    }

    public void set_links(TeamsLink _links) {
        this._links = _links;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCrestURI() {
        return crestURI;
    }

    public void setCrestURI(String crestURI) {
        this.crestURI = crestURI;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(int goalDifference) {
        this.goalDifference = goalDifference;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public HomeAwayStatistics getHome() {
        return home;
    }

    public void setHome(HomeAwayStatistics home) {
        this.home = home;
    }

    public HomeAwayStatistics getAway() {
        return away;
    }

    public void setAway(HomeAwayStatistics away) {
        this.away = away;
    }
}
