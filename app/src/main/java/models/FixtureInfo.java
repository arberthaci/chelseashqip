package models;

/**
 * Created by arber on 16/10/2016.
 */

public class FixtureInfo {
    private Fixture fixture;
    private HeadToHead head2head;

    public Fixture getFixture() {
        return fixture;
    }

    public void setFixture(Fixture fixture) {
        this.fixture = fixture;
    }

    public HeadToHead getHead2head() {
        return head2head;
    }

    public void setHead2head(HeadToHead head2head) {
        this.head2head = head2head;
    }
}
