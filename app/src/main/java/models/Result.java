package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class Result {
    @SerializedName("goalsHomeTeam")
    private int goalsHomeTeam;
    @SerializedName("goalsAwayTeam")
    private int goalsAwayTeam;

    public int getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public void setGoalsHomeTeam(int goalsHomeTeam) {
        this.goalsHomeTeam = goalsHomeTeam;
    }

    public int getGoalsAwayTeam() {
        return goalsAwayTeam;
    }

    public void setGoalsAwayTeam(int goalsAwayTeam) {
        this.goalsAwayTeam = goalsAwayTeam;
    }
}
