package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by root on 16-03-17.
 */
public class TeamsOfLeague {
    private SelfSoccerseasonLink _links;
    @SerializedName("count")
    private int count;
    private ArrayList<Team> teams;

    public SelfSoccerseasonLink get_links() {
        return _links;
    }

    public void set_links(SelfSoccerseasonLink _links) {
        this._links = _links;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }
}
