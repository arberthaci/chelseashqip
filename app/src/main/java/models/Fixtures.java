package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arber Thaci on 3/22/2016.
 */
public class Fixtures {
    private SelfTeamLink _links;
    @SerializedName("count")
    private int count;
    private ArrayList<Fixture> fixtures;

    public SelfTeamLink get_links() {
        return _links;
    }

    public void set_links(SelfTeamLink _links) {
        this._links = _links;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Fixture> getFixtures() {
        return fixtures;
    }

    public void setFixtures(ArrayList<Fixture> fixtures) {
        this.fixtures = fixtures;
    }
}
