package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 16-03-17.
 */
public class Hyperlink {
    @SerializedName("href")
    protected String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
