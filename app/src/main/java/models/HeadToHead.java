package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by arber on 16/10/2016.
 */

public class HeadToHead {
    @SerializedName("count")
    private int count;
    @SerializedName("timeFrameStart")
    private Date timeFrameStart;
    @SerializedName("timeFrameEnd")
    private Date timeFrameEnd;
    @SerializedName("homeTeamWins")
    private int homeTeamWins;
    @SerializedName("awayTeamWins")
    private int awayTeamWins;
    @SerializedName("draws")
    private int draws;
    private Fixture lastHomeWinHomeTeam;
    private Fixture lastWinHomeTeam;
    private Fixture lastAwayWinAwayTeam;
    private Fixture lastWinAwayTeam;
    private ArrayList<Fixture> fixtures;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Date getTimeFrameStart() {
        return timeFrameStart;
    }

    public void setTimeFrameStart(Date timeFrameStart) {
        this.timeFrameStart = timeFrameStart;
    }

    public Date getTimeFrameEnd() {
        return timeFrameEnd;
    }

    public void setTimeFrameEnd(Date timeFrameEnd) {
        this.timeFrameEnd = timeFrameEnd;
    }

    public int getHomeTeamWins() {
        return homeTeamWins;
    }

    public void setHomeTeamWins(int homeTeamWins) {
        this.homeTeamWins = homeTeamWins;
    }

    public int getAwayTeamWins() {
        return awayTeamWins;
    }

    public void setAwayTeamWins(int awayTeamWins) {
        this.awayTeamWins = awayTeamWins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public Fixture getLastHomeWinHomeTeam() {
        return lastHomeWinHomeTeam;
    }

    public void setLastHomeWinHomeTeam(Fixture lastHomeWinHomeTeam) {
        this.lastHomeWinHomeTeam = lastHomeWinHomeTeam;
    }

    public Fixture getLastWinHomeTeam() {
        return lastWinHomeTeam;
    }

    public void setLastWinHomeTeam(Fixture lastWinHomeTeam) {
        this.lastWinHomeTeam = lastWinHomeTeam;
    }

    public Fixture getLastAwayWinAwayTeam() {
        return lastAwayWinAwayTeam;
    }

    public void setLastAwayWinAwayTeam(Fixture lastAwayWinAwayTeam) {
        this.lastAwayWinAwayTeam = lastAwayWinAwayTeam;
    }

    public Fixture getLastWinAwayTeam() {
        return lastWinAwayTeam;
    }

    public void setLastWinAwayTeam(Fixture lastWinAwayTeam) {
        this.lastWinAwayTeam = lastWinAwayTeam;
    }

    public ArrayList<Fixture> getFixtures() {
        return fixtures;
    }

    public void setFixtures(ArrayList<Fixture> fixtures) {
        this.fixtures = fixtures;
    }
}
