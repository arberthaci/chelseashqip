package engine.widget.circularprogressbutton;

public interface OnAnimationEndListener {

    void onAnimationEnd();
}
