package engine;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import models.Article;
import models.FixtureInfo;
import models.Fixtures;
import models.LeagueTable;
import models.Players;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by root on 16-03-21.
 */
public class athEngine {
    public static String ATH_TEAM = "Chelsea FC";
    public static String ATH_LEAGUE_ID = "PL";
    public static String ATH_LEAGUE_NAME = "Premier League";
    public static String ATH_LEAGUE_CUP_NAME = "FA Cup";
    public static String ATH_EUROPEAN_LEAGUE_ID = "CL";
    public static String ATH_EUROPEAN_LEAGUE_NAME = "Champions League";
    public static String ATH_TAG = "ath_app_football";
    public final static String ATH_ONE_OFF_GCM_TAG = "ath_one_off_gcm";
    public final static String ATH_PERIODIC_GCM_TAG = "ath_periodic_gcm";
    public static String ATH_HTTP_HEADER_NAME = "X-Auth-Token";
    public static String ATH_HTTP_HEADER_VALUE = "5c0ce6e40c494969a32589ecd8624adc";
    public static String ALL_LEAGUES_URL = "http://api.football-data.org/v1/soccerseasons";
    public static String LEAGUE_COMPETITION_ID_URL = "";
    public static String EUROPEAN_LEAGUE_COMPETITION_ID_URL = "";
    public static String ALL_TEAMS_BY_LEAGUE_ID_URL = "";
    public static String LEAGUE_TABLE_BY_LEAGUE_ID_URL = "";
    public static String TEAM_INFO_BY_TEAM_ID_URL = "";
    public static String FIXTURES_BY_TEAM_ID_URL = "";
    public static String FIXTURE_INFO_BY_FIXTURE_ID_URL = "";
    public static String PLAYERS_BY_TEAM_ID_URL = "";
    public static String TEAM_CREST_URL = "https://upload.wikimedia.org/wikipedia/en/thumb/c/cc/Chelsea_FC.svg/768px-Chelsea_FC.svg.png";
    public static String TEAM_MARKET_VALUE = "";
    public static String APP_WEBSITE_URL = "http://www.chelseafc.al";
    public static String NEWS_WEBSITE_URL = "http://www.chelseafc.al/?page_id=350";
    public static String THUMBNAIL_WEBSITE_URL = "http://chelseafc.al/android-thumbnail/";
    public static final int HOME_MATCH = 1;
    public static final int AWAY_MATCH = 2;
    public static final String MATCH_FINISHED = "FINISHED";
    public static final String MATCH_SCHEDULED = "SCHEDULED";
    public static final String MATCH_TIMED = "TIMED";
    public static final int TIME_TYPE_DATETIME = 1;
    public static final int TIME_TYPE_DATE_ONLY = 2;
    private static HashMap<String, String> teamNames;

    // Fragment's tag
    public final static String NEWS_FRAGMENT_TAG = "news_ft";
    public final static String FIXTURES_AND_RESULTS_FRAGMENT_TAG = "fixtures_and_results_ft";
    public final static String LAST_TEN_MEETINGS_TAG = "last_ten_meetings_ft";
    public final static String TABLE_FRAGMENT_TAG = "table_ft";
    public final static String TEAM_FRAGMENT_TAG = "team_ft";
    public final static String ABOUT_US_FRAGMENT_TAG = "about_us_ft";
    public final static String FEEDBACK_FRAGMENT_TAG = "feedback_ft";

    // Cashed data
    private static Fixtures fixturesAndResults;
    private static FixtureInfo fixtureInfo;
    private static String homeTeamName;
    private static String awayTeamName;
    private static LeagueTable leagueTable;
    private static Players players;
    private static ArrayList<Article> articles;

    public static InputStream retrieveStream(String url) {
        OkHttpClient client = new OkHttpClient();
        try {
            Request request = new Request.Builder()
                    .addHeader(ATH_HTTP_HEADER_NAME, ATH_HTTP_HEADER_VALUE)
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                Log.i(ATH_TAG, "Error " + response.code() + " for URL " + url);
                return null;
            }
            return response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(ATH_TAG, "Error for URL " + url, e);
        }

        return null;
    }

    public static String getLeagueCompetitionIdUrl() {
        return LEAGUE_COMPETITION_ID_URL;
    }

    public static void setLeagueCompetitionIdUrl(String leagueCompetitionIdUrl) {
        LEAGUE_COMPETITION_ID_URL = leagueCompetitionIdUrl;
    }

    public static String getEuropeanLeagueCompetitionIdUrl() {
        return EUROPEAN_LEAGUE_COMPETITION_ID_URL;
    }

    public static void setEuropeanLeagueCompetitionIdUrl(String europeanLeagueCompetitionIdUrl) {
        EUROPEAN_LEAGUE_COMPETITION_ID_URL = europeanLeagueCompetitionIdUrl;
    }

    public static void setAllTeamsByLeagueIdUrl(String newAllTeamsByLeagueIdUrl) {
        ALL_TEAMS_BY_LEAGUE_ID_URL = newAllTeamsByLeagueIdUrl;
    }

    public static void setLeagueTableByLeagueIdUrl(String newLeagueTableByLeagueIdUrl) {
        LEAGUE_TABLE_BY_LEAGUE_ID_URL = newLeagueTableByLeagueIdUrl;
    }

    public static void setTeamInfoByTeamIdUrl(String newTeamInfoByTeamIdUrl) {
        TEAM_INFO_BY_TEAM_ID_URL = newTeamInfoByTeamIdUrl;
    }

    public static void setFixturesByTeamIdUrl(String newFixturesByTeamIdUrl) {
        FIXTURES_BY_TEAM_ID_URL = newFixturesByTeamIdUrl;
    }

    public static void setFixtureInfoByFixtureIdUrl(String fixtureInfoByFixtureIdUrl) {
        FIXTURE_INFO_BY_FIXTURE_ID_URL = fixtureInfoByFixtureIdUrl;
    }

    public static void setPlayersByTeamIdUrl(String newPlayersByTeamIdUrl) {
        PLAYERS_BY_TEAM_ID_URL = newPlayersByTeamIdUrl;
    }

    public static void setTeamCrestUrl(String newTeamCrestUrl) {
        TEAM_CREST_URL = newTeamCrestUrl;
    }

    public static void setTeamMarketValue(String newTeamMarketValue) {
        TEAM_MARKET_VALUE = newTeamMarketValue;
    }

    public static FixtureInfo getFixtureInfo() {
        return fixtureInfo;
    }

    public static void setFixtureInfo(FixtureInfo fixtureInfo) {
        athEngine.fixtureInfo = fixtureInfo;
    }

    public static String getHomeTeamName() {
        return homeTeamName;
    }

    public static void setHomeTeamName(String homeTeamName) {
        athEngine.homeTeamName = homeTeamName;
    }

    public static String getAwayTeamName() {
        return awayTeamName;
    }

    public static void setAwayTeamName(String awayTeamName) {
        athEngine.awayTeamName = awayTeamName;
    }

    public static Fixtures getFixturesAndResults() {
        return fixturesAndResults;
    }

    public static void setFixturesAndResults(Fixtures fixturesAndResults) {
        athEngine.fixturesAndResults = fixturesAndResults;
    }

    public static LeagueTable getLeagueTable() {
        return leagueTable;
    }

    public static void setLeagueTable(LeagueTable leagueTable) {
        athEngine.leagueTable = leagueTable;
    }

    public static Players getPlayers() {
        return players;
    }

    public static void setPlayers(Players players) {
        athEngine.players = players;
    }

    public static ArrayList<Article> getArticles() {
        return articles;
    }

    public static void setArticles(ArrayList<Article> articles) {
        athEngine.articles = articles;
    }

    public static void initializeTeamNames() {
        if (teamNames == null) {
            teamNames = new HashMap<String, String>();
            // Premier League and Championship teams
            teamNames.put("Chelsea FC", "Chelsea");
            teamNames.put("Manchester United FC", "Man Utd");
            teamNames.put("Tottenham Hotspur FC", "Tottenham");
            teamNames.put("AFC Bournemouth", "Bournemouth");
            teamNames.put("Aston Villa FC", "Aston Villa");
            teamNames.put("Everton FC", "Everton");
            teamNames.put("Watford FC", "Watford");
            teamNames.put("Leicester City FC", "Leicester");
            teamNames.put("Sunderland AFC", "Sunderland");
            teamNames.put("Norwich City FC", "Norwich");
            teamNames.put("Swansea City FC", "Swansea");
            teamNames.put("Newcastle United FC", "Newcastle");
            teamNames.put("Southampton FC", "Southampton");
            teamNames.put("Arsenal FC", "Arsenal");
            teamNames.put("West Ham United FC", "West Ham");
            teamNames.put("Stoke City FC", "Stoke");
            teamNames.put("Liverpool FC", "Liverpool");
            teamNames.put("West Bromwich Albion FC", "West Brom");
            teamNames.put("Manchester City FC", "Man City");
            teamNames.put("Crystal Palace FC", "Crystal");
            teamNames.put("Burnley FC", "Burnley");
            teamNames.put("Brighton & Hove Albion", "Brighton");
            teamNames.put("Middlesbrough FC", "Middlesbrough");
            teamNames.put("Hull City FC", "Hull City");
            teamNames.put("Derby County FC", "Derby");
            teamNames.put("Sheffield Wednesday FC", "Sheff Wed");
            teamNames.put("Cardiff City FC", "Cardiff");
            teamNames.put("Ipswich Town FC", "Ipswich");
            teamNames.put("Birmingham City FC", "Birmingham");
            teamNames.put("Preston North End FC", "Preston");
            teamNames.put("Queens Park Rangers FC", "QPR");
            teamNames.put("Wolverhampton Wanderers FC", "Wolves");
            teamNames.put("Leeds United FC", "Leeds");
            teamNames.put("Blackburn Rovers FC", "Blackburn");
            teamNames.put("Nottingham Forest FC", "Nottingham");
            teamNames.put("Huddersfield Town", "Huddersfield");
            teamNames.put("Reading FC", "Reading");
            teamNames.put("Brentford FC", "Brentford");
            teamNames.put("Bristol City FC", "Bristol");
            teamNames.put("Rotherham United FC", "Rotherham");
            teamNames.put("Fulham FC", "Fulham");
            teamNames.put("Milton Keynes Dons FC", "MK Dons");
            teamNames.put("Charlton Athletic FC", "Charlton");
            teamNames.put("Bolton Wanderers FC", "Bolton");
            // Champions League teams
            teamNames.put("Club Atlético de Madrid", "Atl Madrid");
            teamNames.put("AS Roma", "AS Roma");
            teamNames.put("Karabukspor", "Qarabag");

            teamNames.put("SL Benfica", "Benfica");
            teamNames.put("CSKA Moscow", "CSKA Moscow");
            teamNames.put("Olympiacos F.C.", "Olympiacos");
            teamNames.put("Sporting CP", "Sporting");
            teamNames.put("FC Barcelona", "Barcelona");
            teamNames.put("Juventus Turin", "Juventus");
            teamNames.put("Celtic FC", "Celtic");
            teamNames.put("Paris Saint-Germain", "PSG");
            teamNames.put("FC Bayern München", "Bayern");
            teamNames.put("RSC Anderlecht", "Anderlecht");
            teamNames.put("FC Basel", "Basel");
            teamNames.put("Real Madrid CF", "Real Madrid");
            teamNames.put("APOEL Nicosia", "APOEL");
            teamNames.put("Red Bull Leipzig", "Leipzig");
            teamNames.put("AS Monaco FC", "Monaco");
            teamNames.put("FC Porto", "Porto");
            teamNames.put("Besiktas JK", "Besiktas");
            teamNames.put("Shakhtar Donetsk", "Shakhtar");
            teamNames.put("SSC Napoli", "Napoli");
            teamNames.put("Feyenoord Rotterdam", "Feyenoord");
            teamNames.put("Karabukspor", "Qarabag");
            teamNames.put("Karabukspor", "Qarabag");
            teamNames.put("Karabukspor", "Qarabag");
            teamNames.put("Karabukspor", "Qarabag");
            teamNames.put("Karabukspor", "Qarabag");
            teamNames.put("Karabukspor", "Qarabag");
            teamNames.put("Karabukspor", "Qarabag");
            teamNames.put("Karabukspor", "Qarabag");
        }
    }

    public static String getShortTeamName(String longTeamName) {
        String teamNameToBeReturned = "";
        teamNameToBeReturned = teamNames.get(longTeamName);
        if (teamNameToBeReturned == null) {
            teamNameToBeReturned = longTeamName;
        }
        return teamNameToBeReturned;
    }

    public static String getFormattedDate(String plainDate, int type) {
        String formattedDate = "";
        try {
            if (type == TIME_TYPE_DATETIME) {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date date = formatter.parse(plainDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                formattedDate = String.valueOf(calendar.get(Calendar.DATE)) + " " + getMonthName(calendar.get(Calendar.MONTH)) + " " + String.valueOf(calendar.get(Calendar.YEAR));
                formattedDate = formattedDate + ", " + String.valueOf(calendar.get(Calendar.HOUR_OF_DAY) + 1) + ":" + String.format("%-2s", calendar.get(Calendar.MINUTE)).replace(' ', '0');
            }
            else if (type == TIME_TYPE_DATE_ONLY) {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse(plainDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                formattedDate = String.valueOf(calendar.get(Calendar.DATE)) + " " + getMonthName(calendar.get(Calendar.MONTH)) + " " + String.valueOf(calendar.get(Calendar.YEAR));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            formattedDate = plainDate;
        }
        return formattedDate;
    }

    public static String getMonthName(int month) {
        String monthStr = "";
        switch (month) {
            case 0:
                monthStr = "Janar";
                break;
            case 1:
                monthStr = "Shkurt";
                break;
            case 2:
                monthStr = "Mars";
                break;
            case 3:
                monthStr = "Prill";
                break;
            case 4:
                monthStr = "Maj";
                break;
            case 5:
                monthStr = "Qershor";
                break;
            case 6:
                monthStr = "Korrik";
                break;
            case 7:
                monthStr = "Gusht";
                break;
            case 8:
                monthStr = "Shtator";
                break;
            case 9:
                monthStr = "Tetor";
                break;
            case 10:
                monthStr = "Nëntor";
                break;
            case 11:
                monthStr = "Dhjetor";
                break;
        }
        return monthStr;
    }

    public static String formatPlayerPosition(String playerPosition) {
        String positionToBeReturned = "";
        switch (playerPosition) {
            case "Keeper":
                positionToBeReturned = "Portier";
                break;
            case "Right-Back":
                positionToBeReturned = "Mbrojtës kr. djathtë";
                break;
            case "Centre-Back":
                positionToBeReturned = "Mbrojtës qëndror";
                break;
            case "Left-Back":
                positionToBeReturned = "Mbrojës kr. majtë";
                break;
            case "Defensive Midfield":
                positionToBeReturned = "Mesfushor defensiv";
                break;
            case "Central Midfield":
                positionToBeReturned = "Mesfushor qëndror";
                break;
            case "Attacking Midfield":
                positionToBeReturned = "Mesfushor i avancuar";
                break;
            case "Right Wing":
                positionToBeReturned = "Mesfushor kr. djathtë";
                break;
            case "Left Wing":
                positionToBeReturned = "Mesfushor kr. majtë";
                break;
            case "Centre-Forward":
                positionToBeReturned = "Sulmues qëndror";
                break;
        }
        return positionToBeReturned;
    }

    public static int checkIfHomeorAwayMatch(String homeTeamName, String awayTeamName) {
        int homeOrAway = -1;
        if (homeTeamName.equalsIgnoreCase(ATH_TEAM)) {
            homeOrAway = HOME_MATCH;
        }
        else if (awayTeamName.equalsIgnoreCase(ATH_TEAM)) {
            homeOrAway = AWAY_MATCH;
        }
        return homeOrAway;
    }

    public static String getAlbanianMonthName(String englishMonthName) {
        String albanianMonthName = "";
        if (englishMonthName.equalsIgnoreCase("January")) {
            albanianMonthName = "Janar";
        }
        else if (englishMonthName.equalsIgnoreCase("February")) {
            albanianMonthName = "Shkurt";
        }
        else if (englishMonthName.equalsIgnoreCase("March")) {
            albanianMonthName = "Mars";
        }
        else if (englishMonthName.equalsIgnoreCase("April")) {
            albanianMonthName = "Prill";
        }
        else if (englishMonthName.equalsIgnoreCase("May")) {
            albanianMonthName = "Maj";
        }
        else if (englishMonthName.equalsIgnoreCase("June")) {
            albanianMonthName = "Qershor";
        }
        else if (englishMonthName.equalsIgnoreCase("July")) {
            albanianMonthName = "Korrik";
        }
        else if (englishMonthName.equalsIgnoreCase("August")) {
            albanianMonthName = "Gusht";
        }
        else if (englishMonthName.equalsIgnoreCase("September")) {
            albanianMonthName = "Shtator";
        }
        else if (englishMonthName.equalsIgnoreCase("October")) {
            albanianMonthName = "Tetor";
        }
        else if (englishMonthName.equalsIgnoreCase("November")) {
            albanianMonthName = "Nëntor";
        }
        else if (englishMonthName.equalsIgnoreCase("December")) {
            albanianMonthName = "Dhjetor";
        }
        return albanianMonthName;
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
