package engine;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.google.android.gms.gcm.TaskParams;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.Calendar;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.IntroActivity;

/**
 * Created by root on 17-04-10.
 */

public class athCustomService extends GcmTaskService {

    private GcmNetworkManager mGcmNetworkManager;
    private SharedPreferences prefs;
    private String oldLink = "";
    private String newLink = "";
    private String newsTitle = "";

    @Override
    public int onRunTask(TaskParams taskParams) {
        switch (taskParams.getTag()) {
            case athEngine.ATH_ONE_OFF_GCM_TAG:
                setPeriodicTask();
                return GcmNetworkManager.RESULT_SUCCESS;
            case athEngine.ATH_PERIODIC_GCM_TAG:
                preparePeriodicTask();
                return GcmNetworkManager.RESULT_SUCCESS;
            default:
                return GcmNetworkManager.RESULT_FAILURE;
        }
    }

    private void preparePeriodicTask() {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        if (3 < hourOfDay && hourOfDay > 5) {
            prefs = getApplicationContext().getSharedPreferences("SharedPreferencesAThEngine", MODE_PRIVATE);
            oldLink = prefs.getString("gcm_link", "");
            new GetAllNews().execute(athEngine.NEWS_WEBSITE_URL);
        }

        setPeriodicTask();
    }

    private void setPeriodicTask() {
        mGcmNetworkManager = GcmNetworkManager.getInstance(this);

        Task task = new PeriodicTask.Builder()
                .setService(athCustomService.class)
                .setPeriod(21600) // 21600 seconds = 360 minuta = 6 ore
                .setFlex(60)
                .setTag(athEngine.ATH_PERIODIC_GCM_TAG)
                .setUpdateCurrent(true)
                .setPersisted(true)
                .build();

        mGcmNetworkManager.schedule(task);
    }

    private void sendNotification(String message) {
        Intent intent = new Intent(this, IntroActivity.class);
        //intent.putExtra(NOTIFICATION_ID, "2493");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.app_icon);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_logo_white)
                .setLargeIcon(largeIcon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.BLUE, 3000, 3000)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void scrapWebSite(String mUrl) {
        try {
            Document document = Jsoup.connect(mUrl).get();

            Elements jSoupArticles = document.getElementById("tc-recent-posts-2").getElementsByTag("li");

            try {
                newsTitle = jSoupArticles.get(0).getElementsByClass("post-data").get(0).getElementsByTag("a").get(0).text();
            }
            catch (Exception e) {
                newsTitle = "";
                Log.i(athEngine.ATH_TAG, "scrapWebSite scrapWebSite title Exception: " + e.toString());
            }

            try {
                newLink = jSoupArticles.get(0).getElementsByClass("post-data").get(0).getElementsByTag("a").get(0).attr("href");
            }
            catch (Exception e) {
                newLink = "error";
                Log.i(athEngine.ATH_TAG, "scrapWebSite scrapWebSite link Exception: " + e.toString());
            }
        }
        catch (Exception e) {
            Log.i(athEngine.ATH_TAG, "athCustomService scrapWebSite Exception: " + e.toString());
        }
    }

    private class GetAllNews extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                scrapWebSite(uri[0]);
            }
            catch (Exception e) {
                Log.i(athEngine.ATH_TAG, "scrapWebSite GetAllNews doInBackground Exception: " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!oldLink.equalsIgnoreCase(newLink)
                    && !newLink.trim().equalsIgnoreCase("error")
                    && !newLink.trim().equalsIgnoreCase("")) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("gcm_link", newLink);
                editor.commit();
                sendNotification(newsTitle);
            }
        }
    }
}
