package al.chelseafc.chelseashqip.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.athMainActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import engine.athEngine;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AboutUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AboutUsFragment extends Fragment {
    private CircleImageView aboutUsLogo;
    private TextView tvDescription;
    private TextView tvInstalls;
    private TextView tvRating;
    private SharedPreferences prefs;
    private String statsInstall = "";
    private String statsRating = "";

    public AboutUsFragment() {}

    public static AboutUsFragment newInstance() {
        return new AboutUsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((athMainActivity) getActivity()).setFragmentTag(athEngine.ABOUT_US_FRAGMENT_TAG, getResources().getString(R.string.menu_item_about_us));
        ((athMainActivity) getActivity()).closeKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        aboutUsLogo = (CircleImageView) view.findViewById(R.id.logoAboutUs);
        Picasso.with(getActivity())
                .load(R.drawable.app_icon)
                .placeholder(R.drawable.loading_image)
                .error(R.drawable.loading_image)
                .into(aboutUsLogo);
        tvDescription = (TextView) view.findViewById(R.id.aboutUsDescription);
        tvInstalls = (TextView) view.findViewById(R.id.aboutUsInstall);
        tvRating = (TextView) view.findViewById(R.id.aboutUsRating);
        prefs = getActivity().getSharedPreferences("SharedPreferencesAThEngine", MODE_PRIVATE);
    }

    private void afterInitialization() {
        tvDescription.setMovementMethod(new ScrollingMovementMethod());
        statsInstall = prefs.getString("STATS_INSTALLING", "0");
        statsRating = prefs.getString("STATS_RATING", "0");
        tvInstalls.setText(statsInstall);
        tvRating.setText(statsRating + "/5");
    }
}
