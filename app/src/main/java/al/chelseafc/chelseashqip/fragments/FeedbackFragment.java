package al.chelseafc.chelseashqip.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.RadioButton;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.athMainActivity;
import engine.Functions;
import engine.Mail;
import engine.athEngine;
import engine.widget.circularprogressbutton.CircularProgressButton;
import engine.widget.circularprogressbutton.OnAnimationEndListener;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeedbackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedbackFragment extends Fragment implements View.OnClickListener {

    private CircularProgressButton sendButton;
    private RadioButton optionOne;
    private RadioButton optionTwo;
    private RadioButton optionThree;
    private RadioButton optionFour;
    private EditText feedbackString;
    private String optionString = "";

    public FeedbackFragment() {}

    public static FeedbackFragment newInstance() {
        return new FeedbackFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((athMainActivity) getActivity()).setFragmentTag(athEngine.FEEDBACK_FRAGMENT_TAG, getResources().getString(R.string.menu_item_feedback));
        ((athMainActivity) getActivity()).closeKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendButton:
                if (optionString != null && !optionString.trim().equalsIgnoreCase("")) {
                    sendButtonClick();
                }
                else {
                    ((athMainActivity) getActivity()).showMessageOnSnack(getResources().getString(R.string.ath_engine_snackbar_no_option_selected));
                }
                break;
            case R.id.optionOne:
                optionString = "Opsioni i parë: " + getResources().getString(R.string.feedback_radio_first_option);
                break;
            case R.id.optionTwo:
                optionString = "Opsioni i dytë: " + getResources().getString(R.string.feedback_radio_second_option);
                break;
            case R.id.optionThree:
                optionString = "Opsioni i tretë: " + getResources().getString(R.string.feedback_radio_third_option);
                break;
            case R.id.optionFour:
                optionString = "Opsioni i katërt: " + getResources().getString(R.string.feedback_radio_fourth_option);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        sendButton = (CircularProgressButton) view.findViewById(R.id.sendButton);
        optionOne = (RadioButton) view.findViewById(R.id.optionOne);
        optionTwo = (RadioButton) view.findViewById(R.id.optionTwo);
        optionThree = (RadioButton) view.findViewById(R.id.optionThree);
        optionFour = (RadioButton) view.findViewById(R.id.optionFour);
        feedbackString = (EditText) view.findViewById(R.id.feedbackString);
    }

    private void afterInitialization(){
        sendButton.setIndeterminateProgressMode(true);
        sendButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        sendButton.setOnClickListener(this);

        ViewTreeObserver vto = sendButton.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    sendButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    sendButton.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });

        optionOne.setOnClickListener(this);
        optionTwo.setOnClickListener(this);
        optionThree.setOnClickListener(this);
        optionFour.setOnClickListener(this);
    }

    private void sendButtonClick() {
        sendButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        sendButton.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);

        if (athEngine.isNetworkAvailable(getActivity())) {
            new SendEmail().execute();
        }
        else {
            Thread timer = new Thread() {
            public void run() {
                    try {
                        sleep(750);
                    }
                    catch (InterruptedException e) {
                        Log.i(athEngine.ATH_TAG, "FeedbackFragment thread Exception: " + e.toString());
                    }
                    finally {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sendButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
                                sendButton.setEnabled(true);
                                ((athMainActivity) getActivity()).showMessageOnSnack(String.valueOf(getResources().getString(R.string.ath_engine_snackbar_no_internet_error_message)));
                            }
                        });
                    }
                }
            };
            timer.start();
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            sendButton.clearAnimation();
        }
    };

    private String formatEmailBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("Mesazh i ri nga një përdorues!").append(System.getProperty("line.separator"));
        sb.append("Data: " + android.text.format.DateFormat.format("dd.MM.yyyy HH:mm", new java.util.Date()).toString()).append(System.getProperty("line.separator"));
        sb.append(System.getProperty("line.separator"));

        sb.append(optionString).append(System.getProperty("line.separator"));

        String freeText = feedbackString.getText().toString();
        if (freeText.trim().equalsIgnoreCase("")) {
            sb.append("Mesazhi: Përdoruesi nuk ka shkruar asnjë mesazh.").append(System.getProperty("line.separator"));
        }
        else {
            sb.append("Mesazhi: " + freeText).append(System.getProperty("line.separator"));
        }
        sb.append(System.getProperty("line.separator"));

        sb.append("#CSA").append(System.getProperty("line.separator"));


        return sb.toString();
    }

    private class SendEmail extends AsyncTask<String, String, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean sent) {
            super.onPostExecute(sent);

            if (sent) {
                sendButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                sendButton.setAnimationListener(onAnimationEndListener);
                sendButton.setEnabled(false);
                ((athMainActivity) getActivity()).showMessageOnSnack(String.valueOf(getResources().getString(R.string.ath_engine_snackbar_email_success)));
            }
            else {
                sendButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
                sendButton.setEnabled(true);
                ((athMainActivity) getActivity()).showMessageOnSnack(String.valueOf(getResources().getString(R.string.ath_engine_snackbar_email_failed)));
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {
            boolean sent = false;

            Mail mMail = new Mail(getResources().getString(R.string.email_username), getResources().getString(R.string.email_password), 1);
            String[] toArr = {getResources().getString(R.string.owner_email_username)};
            mMail.setTo(toArr);
            mMail.setFrom(getResources().getString(R.string.email_username));
            mMail.setSubject(getResources().getString(R.string.email_subject));
            mMail.setBody(formatEmailBody());

            try {
                if (mMail.send()) {
                    sent = true;
                }
                else {
                    sent = false;
                }

            }
            catch (Exception e) {
                sent = false;
            }
            return sent;
        }
    }
}
