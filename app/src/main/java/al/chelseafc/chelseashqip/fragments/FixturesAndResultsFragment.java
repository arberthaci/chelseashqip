package al.chelseafc.chelseashqip.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.athMainActivity;
import engine.DividerItemDecoration;
import engine.athEngine;
import models.Fixture;
import models.FixtureInfo;
import models.Fixtures;
import models.Odds;

public class FixturesAndResultsFragment extends Fragment implements View.OnClickListener {
    private RelativeLayout tabFixtures;
    private View tabFixturesLine;
    private RelativeLayout tabResults;
    private View tabResultsLine;
    private RelativeLayout tabAllMatches;
    private View tabAllMatchesLine;
    private RelativeLayout tabHomeMatches;
    private View tabHomeMatchesLine;
    private RelativeLayout tabAwayMatches;
    private View tabAwayMatchesLine;
    private AVLoadingIndicatorView loading;
    LinearLayout noInternetView;
    LinearLayout noDataView;
    private SwipeRefreshLayout swipeToRefresh;
    private Fixtures fixturesAndResults;
    private static FixtureInfo fixtureInfo;
    private static Odds nexFixtureOdds;
    private ArrayList<Fixture> matches;
    private ArrayList<Fixture> allFixtures;
    private ArrayList<Fixture> homeFixtures;
    private ArrayList<Fixture> awayFixtures;
    private ArrayList<Fixture> allResults;
    private ArrayList<Fixture> homeResults;
    private ArrayList<Fixture> awayResults;
    private RecyclerView rvOfMatches;
    private CustomListAdapter matchesAdapter;
    private static final int IS_FIXTURE = 1;
    private static final int IS_RESULT = 2;
    private int selectedTabType = IS_FIXTURE;
    private boolean showFixtureInfo = true;
    private int nextMatch = -1;
    private boolean noInternet = false;

    public FixturesAndResultsFragment() {}

    public static FixturesAndResultsFragment newInstance() {
        return new FixturesAndResultsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fixturesAndResults = athEngine.getFixturesAndResults();
        fixtureInfo = athEngine.getFixtureInfo();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((athMainActivity) getActivity()).setFragmentTag(athEngine.FIXTURES_AND_RESULTS_FRAGMENT_TAG, getResources().getString(R.string.menu_item_fixtures_and_results));
        ((athMainActivity) getActivity()).closeKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fixtures_and_results, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();

        if (!athEngine.FIXTURES_BY_TEAM_ID_URL.equalsIgnoreCase("")) {
            athEngine.initializeTeamNames();
            if (fixturesAndResults == null) {
                if (athEngine.isNetworkAvailable(getActivity())) {
                    noInternet = false;
                    new GetFixturesAndResults().execute(athEngine.FIXTURES_BY_TEAM_ID_URL);
                }
                else {
                    noInternet = true;
                    rvOfMatches.setVisibility(View.GONE);
                    noDataView.setVisibility(View.GONE);
                    noInternetView.setVisibility(View.VISIBLE);
                }
            }
            else {
                if (fixturesAndResults.getFixtures().size() == 0) {
                    rvOfMatches.setVisibility(View.GONE);
                    loading.hide();
                    noInternetView.setVisibility(View.GONE);
                    noDataView.setVisibility(View.VISIBLE);
                }
                else {
                    prepareRecycleView(fixturesAndResults);
                }
            }
        }
        else {
            rvOfMatches.setVisibility(View.GONE);
            loading.hide();
            noInternetView.setVisibility(View.GONE);
            noDataView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tabFixtures:
                if (selectedTabType == IS_RESULT) {
                    tabFixturesLine.setVisibility(View.VISIBLE);
                    tabResultsLine.setVisibility(View.GONE);
                    selectedTabType = IS_FIXTURE;
                    tabAllMatches.performClick();
                }
                break;
            case R.id.tabResults:
                if (selectedTabType == IS_FIXTURE) {
                    tabFixturesLine.setVisibility(View.GONE);
                    tabResultsLine.setVisibility(View.VISIBLE);
                    selectedTabType = IS_RESULT;
                    tabAllMatches.performClick();
                }
                break;
            case R.id.tabAllMatches:
                if (!noInternet) {
                    tabAllMatchesLine.setVisibility(View.VISIBLE);
                    tabHomeMatchesLine.setVisibility(View.GONE);
                    tabAwayMatchesLine.setVisibility(View.GONE);
                    if (selectedTabType == IS_FIXTURE) {
                        showFixtureInfo = true;
                        refreshRecycleView(allFixtures);
                    } else if (selectedTabType == IS_RESULT) {
                        refreshRecycleView(allResults);
                    }
                }
                break;
            case R.id.tabHomeMatches:
                if (!noInternet) {
                    tabAllMatchesLine.setVisibility(View.GONE);
                    tabHomeMatchesLine.setVisibility(View.VISIBLE);
                    tabAwayMatchesLine.setVisibility(View.GONE);
                    if (selectedTabType == IS_FIXTURE) {
                        if (nextMatch == athEngine.HOME_MATCH) {
                            showFixtureInfo = true;
                        } else {
                            showFixtureInfo = false;
                        }
                        refreshRecycleView(homeFixtures);
                    } else if (selectedTabType == IS_RESULT) {
                        refreshRecycleView(homeResults);
                    }
                }
                break;
            case R.id.tabAwayMatches:
                if (!noInternet) {
                    tabAllMatchesLine.setVisibility(View.GONE);
                    tabHomeMatchesLine.setVisibility(View.GONE);
                    tabAwayMatchesLine.setVisibility(View.VISIBLE);
                    if (selectedTabType == IS_FIXTURE) {
                        if (nextMatch == athEngine.AWAY_MATCH) {
                            showFixtureInfo = true;
                        } else {
                            showFixtureInfo = false;
                        }
                        refreshRecycleView(awayFixtures);
                    } else if (selectedTabType == IS_RESULT) {
                        refreshRecycleView(awayResults);
                    }
                }
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        tabFixtures = (RelativeLayout) view.findViewById(R.id.tabFixtures);
        tabFixturesLine = (View) view.findViewById(R.id.tabFixturesLine);
        tabResults = (RelativeLayout) view.findViewById(R.id.tabResults);
        tabResultsLine = (View) view.findViewById(R.id.tabResultsLine);
        tabAllMatches = (RelativeLayout) view.findViewById(R.id.tabAllMatches);
        tabAllMatchesLine = (View) view.findViewById(R.id.tabAllMatchesLine);
        tabHomeMatches = (RelativeLayout) view.findViewById(R.id.tabHomeMatches);
        tabHomeMatchesLine = (View) view.findViewById(R.id.tabHomeMatchesLine);
        tabAwayMatches = (RelativeLayout) view.findViewById(R.id.tabAwayMatches);
        tabAwayMatchesLine = (View) view.findViewById(R.id.tabAwayMatchesLine);
        noDataView = (LinearLayout) view.findViewById(R.id.llNoDataNews);
        noInternetView = (LinearLayout) view.findViewById(R.id.llNoInternetNews);
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_to_refresh);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.loadingFixturesAndResults);
        rvOfMatches = (RecyclerView) view.findViewById(R.id.rvOfMatches);
    }

    private void afterInitialization() {
        tabFixturesLine.setVisibility(View.VISIBLE);
        tabResultsLine.setVisibility(View.GONE);
        tabAllMatchesLine.setVisibility(View.VISIBLE);
        tabHomeMatchesLine.setVisibility(View.GONE);
        tabAwayMatchesLine.setVisibility(View.GONE);

        noDataView.setVisibility(View.GONE);
        noInternetView.setVisibility(View.GONE);

        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (athEngine.isNetworkAvailable(getActivity())) {
                    noInternet = false;
                    tabFixtures.performClick();
                    if (rvOfMatches.getVisibility() == View.GONE) {
                        rvOfMatches.setVisibility(View.GONE);
                        noDataView.setVisibility(View.GONE);
                        noInternetView.setVisibility(View.GONE);
                        loading.show();
                    }
                    new GetFixturesAndResults().execute(athEngine.FIXTURES_BY_TEAM_ID_URL);
                }
                else {
                    noInternet = true;
                    loading.show();
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(1000);
                            }
                            catch (InterruptedException e) {
                                Log.i(athEngine.ATH_TAG, "FixturesAndResultsFragment thread Exception: " + e.toString());
                            }
                            finally {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeToRefresh.setRefreshing(false);
                                        loading.hide();
                                        noInternetView.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }
                    };
                    timer.start();
                    rvOfMatches.setVisibility(View.GONE);
                    noDataView.setVisibility(View.GONE);
                    noInternetView.setVisibility(View.GONE);
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvOfMatches.setLayoutManager(mLayoutManager);
        rvOfMatches.setItemAnimator(new DefaultItemAnimator());
        rvOfMatches.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        tabFixtures.setOnClickListener(this);
        tabResults.setOnClickListener(this);
        tabAllMatches.setOnClickListener(this);
        tabHomeMatches.setOnClickListener(this);
        tabAwayMatches.setOnClickListener(this);
    }

    private void prepareRecycleView(Fixtures mFixture) {
        matches = new ArrayList<>();
        allFixtures = new ArrayList<>();
        homeFixtures = new ArrayList<>();
        awayFixtures = new ArrayList<>();
        allResults = new ArrayList<>();
        homeResults = new ArrayList<>();
        awayResults = new ArrayList<>();

        for (Fixture fixtureIterator : mFixture.getFixtures()) {
            if (fixtureIterator.getStatus().equalsIgnoreCase(athEngine.MATCH_FINISHED)) {
                allResults.add(fixtureIterator);

                switch (athEngine.checkIfHomeorAwayMatch(fixtureIterator.getHomeTeamName(), fixtureIterator.getAwayTeamName())) {
                    case athEngine.HOME_MATCH:
                        homeResults.add(fixtureIterator);
                        break;
                    case athEngine.AWAY_MATCH:
                        awayResults.add(fixtureIterator);
                        break;
                }
            }
            else if (fixtureIterator.getStatus().equalsIgnoreCase(athEngine.MATCH_SCHEDULED) || fixtureIterator.getStatus().equalsIgnoreCase(athEngine.MATCH_TIMED)) {
                allFixtures.add(fixtureIterator);

                switch (athEngine.checkIfHomeorAwayMatch(fixtureIterator.getHomeTeamName(), fixtureIterator.getAwayTeamName())) {
                    case athEngine.HOME_MATCH:
                        homeFixtures.add(fixtureIterator);
                        if (allFixtures.size() == 1) {
                            nextMatch = athEngine.HOME_MATCH;
                        }
                        break;
                    case athEngine.AWAY_MATCH:
                        awayFixtures.add(fixtureIterator);
                        if (allFixtures.size() == 1) {
                            nextMatch = athEngine.AWAY_MATCH;
                        }
                        break;
                }

                if (allFixtures.size() == 1 && fixtureInfo == null) {
                    athEngine.setFixtureInfoByFixtureIdUrl(fixtureIterator.get_links().getSelf().getHref());
                    if (!athEngine.FIXTURE_INFO_BY_FIXTURE_ID_URL.equalsIgnoreCase("")) {
                        new GetFixtureInfo().execute(athEngine.FIXTURE_INFO_BY_FIXTURE_ID_URL);
                    }
                }
            }
        }

        Collections.reverse(allResults);
        Collections.reverse(homeResults);
        Collections.reverse(awayResults);

        matches = allFixtures;
        matchesAdapter = new CustomListAdapter();
        rvOfMatches.setAdapter(matchesAdapter);

        rvOfMatches.setVisibility(View.VISIBLE);
        swipeToRefresh.setRefreshing(false);
        if (fixtureInfo != null && loading.isShown()) {
            loading.hide();
        }
    }

    private void refreshRecycleView(ArrayList<Fixture> mMatches) {
        matches = mMatches;
        if (matchesAdapter != null) {
            matchesAdapter.notifyDataSetChanged();
        }
        if (loading.isShown()) {
            loading.hide();
        }
    }

    private class GetFixturesAndResults extends AsyncTask<String, String, String> {
        private Fixtures mFixturesAndResults;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream source = athEngine.retrieveStream(uri[0]);
                Gson gson = new Gson();
                if (source != null) {
                    Reader reader = new InputStreamReader(source);
                    mFixturesAndResults = gson.fromJson(reader, Fixtures.class);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                mFixturesAndResults = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mFixturesAndResults != null && mFixturesAndResults.getFixtures() != null) {
                athEngine.setFixturesAndResults(mFixturesAndResults);
                fixturesAndResults = mFixturesAndResults;
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);

                prepareRecycleView(mFixturesAndResults);
            }
            else {
                rvOfMatches.setVisibility(View.GONE);
                loading.hide();
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class GetFixtureInfo extends AsyncTask<String, String, String> {
        private FixtureInfo mFixtureInfo;
        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream source = athEngine.retrieveStream(uri[0]);
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
                if (source != null) {
                    Reader reader = new InputStreamReader(source);
                    mFixtureInfo = gson.fromJson(reader, FixtureInfo.class);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                mFixtureInfo = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (mFixtureInfo != null) {
                athEngine.setFixtureInfo(mFixtureInfo);
                athEngine.setHomeTeamName(matches.get(0).getHomeTeamName());
                athEngine.setAwayTeamName(matches.get(0).getAwayTeamName());
                fixtureInfo = mFixtureInfo;
            }
            if (loading.isShown()) {
                loading.hide();
            }
        }
    }

    private class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.ViewFixturesAndResultsHolder> {
        class ViewFixturesAndResultsHolder extends RecyclerView.ViewHolder {
            LinearLayout row;
            ImageView ivFixtureInfo;
            TextView tvCompetition;
            TextView tvHomeTeam;
            TextView tvResult;
            TextView tvAwayTeam;
            TextView tvDate;

            ViewFixturesAndResultsHolder(View view) {
                super(view);
                row = (LinearLayout) view.findViewById(R.id.rowFixturesAndResults);
                ivFixtureInfo = (ImageView) view.findViewById(R.id.singleFixtureInfo);
                tvCompetition = (TextView) view.findViewById(R.id.singleCompetition);
                tvHomeTeam = (TextView) view.findViewById(R.id.singleHomeTeam);
                tvResult = (TextView) view.findViewById(R.id.singleResult);
                tvAwayTeam = (TextView) view.findViewById(R.id.singleAwayTeam);
                tvDate = (TextView) view.findViewById(R.id.singleDate);
            }
        }

        @Override
        public ViewFixturesAndResultsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_fixtures_and_results, parent, false);
            return new ViewFixturesAndResultsHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewFixturesAndResultsHolder holder, final int position) {
            final Fixture match = matches.get(position);

            holder.ivFixtureInfo.setVisibility(View.GONE);
            if (selectedTabType == IS_FIXTURE && position == 0 && showFixtureInfo) {
                holder.ivFixtureInfo.setVisibility(View.VISIBLE);
                nexFixtureOdds = match.getOdds();
            }

            holder.tvHomeTeam.setText(athEngine.getShortTeamName(match.getHomeTeamName()));
            if (selectedTabType == IS_FIXTURE) {
                holder.tvResult.setText("V");
            }
            else if (selectedTabType == IS_RESULT) {
                holder.tvResult.setText(match.getResult().getGoalsHomeTeam() + " - " + match.getResult().getGoalsAwayTeam());
            }
            holder.tvAwayTeam.setText(athEngine.getShortTeamName(match.getAwayTeamName()));
            holder.tvDate.setText(athEngine.getFormattedDate(match.getDate(), athEngine.TIME_TYPE_DATETIME));

            if (match.getHomeTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                holder.tvHomeTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            }
            else {
                holder.tvHomeTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
            }

            if (match.getAwayTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                holder.tvAwayTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            }
            else {
                holder.tvAwayTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
            }

            String leagueCompetitionId = athEngine.getLeagueCompetitionIdUrl().substring(athEngine.getLeagueCompetitionIdUrl().lastIndexOf("/") + 1);
            String europeanLeagueCompetitionId = athEngine.getEuropeanLeagueCompetitionIdUrl().substring(athEngine.getEuropeanLeagueCompetitionIdUrl().lastIndexOf("/") + 1);
            String currentCompetitionId = match.get_links().getCompetition().getHref().substring(match.get_links().getCompetition().getHref().lastIndexOf("/") + 1);
            if (currentCompetitionId.equalsIgnoreCase(leagueCompetitionId)) {
                holder.tvCompetition.setText(athEngine.ATH_LEAGUE_NAME);
            }
            else if (currentCompetitionId.equalsIgnoreCase(europeanLeagueCompetitionId)) {
                holder.tvCompetition.setText(athEngine.ATH_EUROPEAN_LEAGUE_NAME);
            }
            else {
                holder.tvCompetition.setText(athEngine.ATH_LEAGUE_CUP_NAME);
            }

            if (selectedTabType == IS_FIXTURE) {
                holder.row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (position == 0 && showFixtureInfo) {
                            BottomSheetDialogFragment bottomSheetDialogFragment = new HeadToHeadBottomSheetDialogFragment();
                            bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            int count = 0;
            if (matches != null && matches.size() != 0) {
                count = matches.size();
            }
            return count;
        }
    }

    public static class HeadToHeadBottomSheetDialogFragment extends BottomSheetDialogFragment {

        private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        dismiss();
                        break;
                }
            }


            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        };

        @Override
        public void onDestroyView() {
            super.onDestroyView();
        }

        @Override
        public void setupDialog(Dialog dialog, int style) {
            super.setupDialog(dialog, style);
            View contentView = View.inflate(getContext(), R.layout.fragment_head_to_head, null);
            dialog.setContentView(contentView);

            if (fixtureInfo != null) {
                if (nexFixtureOdds != null) {
                    LinearLayout betView = (LinearLayout) contentView.findViewById(R.id.tvHeadToHeadBetView);
                    betView.setVisibility(View.VISIBLE);

                    TextView homeTeamBetLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadHomeTeamBetLabel);
                    homeTeamBetLabel.setText(athEngine.getShortTeamName(athEngine.getHomeTeamName()));
                    TextView drawBetLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadDrawBetLabel);
                    drawBetLabel.setText("X");
                    TextView awayTeamBetLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadAwayTeamBetLabel);
                    awayTeamBetLabel.setText(athEngine.getShortTeamName(athEngine.getAwayTeamName()));

                    TextView homeTeamBetValue = (TextView) contentView.findViewById(R.id.tvHeadToHeadHomeTeamBetValue);
                    homeTeamBetValue.setText(String.valueOf(nexFixtureOdds.getHomeWin()));
                    TextView drawBetValue = (TextView) contentView.findViewById(R.id.tvHeadToHeadDrawBetValue);
                    drawBetValue.setText(String.valueOf(nexFixtureOdds.getDraw()));
                    TextView awayTeamBetValue = (TextView) contentView.findViewById(R.id.tvHeadToHeadAwayTeamBetValue);
                    awayTeamBetValue.setText(String.valueOf(nexFixtureOdds.getAwayWin()));
                }

                TextView intro = (TextView) contentView.findViewById(R.id.tvHeadToHeadIntro);
                intro.setText("Statistikat e përballjeve midis dy ekipeve në " + fixtureInfo.getHead2head().getCount() + " takimet e fundit:");

                TextView homeTeamWinsLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadHomeTeamWinsLabel);
                homeTeamWinsLabel.setText("Fitore të " + athEngine.getShortTeamName(athEngine.getHomeTeamName()) + "");
                TextView homeTeamWinsValue = (TextView) contentView.findViewById(R.id.tvHeadToHeadHomeTeamWinsValue);
                homeTeamWinsValue.setText(String.valueOf(fixtureInfo.getHead2head().getHomeTeamWins()));
                TextView drawsLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadDrawsLabel);
                drawsLabel.setText("Barazime");
                TextView drawsValue = (TextView) contentView.findViewById(R.id.tvHeadToHeadDrawsValue);
                drawsValue.setText(String.valueOf(fixtureInfo.getHead2head().getDraws()));
                TextView awayTeamWinsLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadAwayTeamWinsLabel);
                awayTeamWinsLabel.setText("Fitore të " + athEngine.getShortTeamName(athEngine.getAwayTeamName()) + "");
                TextView awayTeamWinsValue = (TextView) contentView.findViewById(R.id.tvHeadToHeadAwayTeamWinsValue);
                awayTeamWinsValue.setText(String.valueOf(fixtureInfo.getHead2head().getAwayTeamWins()));

                if (fixtureInfo.getHead2head().getHomeTeamWins() != 0) {
                    TextView lastWinHomeTeamLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinHomeTeamLabel);
                    lastWinHomeTeamLabel.setText("Fitorja e fundit e " + athEngine.getShortTeamName(athEngine.getHomeTeamName()) + ":");
                    TextView lastWinHomeTeamDate = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinHomeTeamDate);
                    lastWinHomeTeamDate.setText(athEngine.getFormattedDate(fixtureInfo.getHead2head().getLastWinHomeTeam().getDate(), athEngine.TIME_TYPE_DATE_ONLY));
                    TextView lastWinHomeTeamNameHome = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinHomeTeamNameHome);
                    lastWinHomeTeamNameHome.setText(athEngine.getShortTeamName(fixtureInfo.getHead2head().getLastWinHomeTeam().getHomeTeamName()));
                    if (fixtureInfo.getHead2head().getLastWinHomeTeam().getHomeTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                        lastWinHomeTeamNameHome.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    }
                    TextView lastWinHomeTeamResult = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinHomeTeamResult);
                    lastWinHomeTeamResult.setText(fixtureInfo.getHead2head().getLastWinHomeTeam().getResult().getGoalsHomeTeam() + " - " + fixtureInfo.getHead2head().getLastWinHomeTeam().getResult().getGoalsAwayTeam());
                    TextView lastWinHomeTeamNameAway = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinHomeTeamNameAway);
                    lastWinHomeTeamNameAway.setText(athEngine.getShortTeamName(fixtureInfo.getHead2head().getLastWinHomeTeam().getAwayTeamName()));
                    if (fixtureInfo.getHead2head().getLastWinHomeTeam().getAwayTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                        lastWinHomeTeamNameAway.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    }
                }
                else {
                    LinearLayout llHeadToHeadLastWinHomeTeam = (LinearLayout) contentView.findViewById(R.id.llHeadToHeadLastWinHomeTeam);
                    llHeadToHeadLastWinHomeTeam.setVisibility(View.GONE);
                }

                if (fixtureInfo.getHead2head().getAwayTeamWins() != 0) {
                    TextView lastWinAwayTeamLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinAwayTeamLabel);
                    lastWinAwayTeamLabel.setText("Fitorja e fundit e " + athEngine.getShortTeamName(athEngine.getAwayTeamName()) + ":");
                    TextView lastWinAwayTeamDate = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinAwayTeamDate);
                    lastWinAwayTeamDate.setText(athEngine.getFormattedDate(fixtureInfo.getHead2head().getLastWinAwayTeam().getDate(), athEngine.TIME_TYPE_DATE_ONLY));
                    TextView lastWinAwayTeamNameHome = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinAwayTeamNameHome);
                    lastWinAwayTeamNameHome.setText(athEngine.getShortTeamName(fixtureInfo.getHead2head().getLastWinAwayTeam().getHomeTeamName()));
                    if (fixtureInfo.getHead2head().getLastWinAwayTeam().getHomeTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                        lastWinAwayTeamNameHome.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    }
                    TextView lastWinAwayTeamResult = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinAwayTeamResult);
                    lastWinAwayTeamResult.setText(fixtureInfo.getHead2head().getLastWinAwayTeam().getResult().getGoalsHomeTeam() + " - " + fixtureInfo.getHead2head().getLastWinAwayTeam().getResult().getGoalsAwayTeam());
                    TextView lastWinAwayTeamNameAway = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastWinAwayTeamNameAway);
                    lastWinAwayTeamNameAway.setText(athEngine.getShortTeamName(fixtureInfo.getHead2head().getLastWinAwayTeam().getAwayTeamName()));
                    if (fixtureInfo.getHead2head().getLastWinAwayTeam().getAwayTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                        lastWinAwayTeamNameAway.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    }
                }
                else {
                    LinearLayout llHeadToHeadLastWinAwayTeam = (LinearLayout) contentView.findViewById(R.id.llHeadToHeadLastWinAwayTeam);
                    llHeadToHeadLastWinAwayTeam.setVisibility(View.GONE);
                }

                LinearLayout lastTenMeetings = (LinearLayout) contentView.findViewById(R.id.llHeadToHeadLastTenMeetings);
                lastTenMeetings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onDestroyView();
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                                .add(R.id.ath_fragment_container, LastTenMeetingsFragment.newInstance(), athEngine.LAST_TEN_MEETINGS_TAG)
                                .commit();
                    }
                });
                TextView lastTenMeetingsLabel = (TextView) contentView.findViewById(R.id.tvHeadToHeadLastTenMeetingsLabel);
                lastTenMeetingsLabel.setText(fixtureInfo.getHead2head().getCount() + " takimet e fundit");
            }

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();

            if( behavior != null && behavior instanceof BottomSheetBehavior ) {
                ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
                ((BottomSheetBehavior) behavior).setPeekHeight((int) getResources().getDimension(R.dimen.bottom_sheet_peek_height_head_to_head));
            }
        }
    }
}
