package al.chelseafc.chelseashqip.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.athMainActivity;
import engine.DividerItemDecoration;
import engine.athEngine;
import models.LeagueTable;
import models.Standing;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TableFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TableFragment extends Fragment {
    private LeagueTable leagueTable;
    private ArrayList<Standing> standings;
    private static Standing athTeamStanding;
    private SwipeRefreshLayout swipeToRefresh;
    private AVLoadingIndicatorView loading;
    LinearLayout noInternetView;
    LinearLayout noDataView;
    private RecyclerView rvLeagueTable;
    private CustomListAdapter adapter;
    private LinearLayout tableHeader;
    private TextView leagueName;

    public TableFragment() {}

    public static TableFragment newInstance() {
        return new TableFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        leagueTable = athEngine.getLeagueTable();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((athMainActivity) getActivity()).setFragmentTag(athEngine.TABLE_FRAGMENT_TAG, getResources().getString(R.string.menu_item_table));
        ((athMainActivity) getActivity()).closeKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_table, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();

        if (!athEngine.ALL_TEAMS_BY_LEAGUE_ID_URL.equalsIgnoreCase("")) {
            athEngine.initializeTeamNames();
            if (leagueTable == null) {
                if (athEngine.isNetworkAvailable(getActivity())) {
                    new GetLeagueTable().execute(athEngine.LEAGUE_TABLE_BY_LEAGUE_ID_URL);
                }
                else {
                    tableHeader.setVisibility(View.GONE);
                    rvLeagueTable.setVisibility(View.GONE);
                    noDataView.setVisibility(View.GONE);
                    noInternetView.setVisibility(View.VISIBLE);
                }
            }
            else {
                if (leagueTable.getStanding().size() == 0) {
                    tableHeader.setVisibility(View.GONE);
                    rvLeagueTable.setVisibility(View.GONE);
                    loading.hide();
                    noInternetView.setVisibility(View.GONE);
                    noDataView.setVisibility(View.VISIBLE);
                }
                else {
                    prepareRecycleView(leagueTable);
                }
            }
        }
        else {
            tableHeader.setVisibility(View.GONE);
            rvLeagueTable.setVisibility(View.GONE);
            loading.hide();
            noInternetView.setVisibility(View.GONE);
            noDataView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.loadingTable);
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_to_refresh);
        rvLeagueTable = (RecyclerView) view.findViewById(R.id.listLeagueTable);
        tableHeader = (LinearLayout) view.findViewById(R.id.headerLeagueTable);
        leagueName = (TextView) view.findViewById(R.id.leagueNameTable);
        noDataView = (LinearLayout) view.findViewById(R.id.llNoDataNews);
        noInternetView = (LinearLayout) view.findViewById(R.id.llNoInternetNews);
    }

    private void afterInitialization() {
        tableHeader.setVisibility(View.GONE);
        noDataView.setVisibility(View.GONE);
        noInternetView.setVisibility(View.GONE);

        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (athEngine.isNetworkAvailable(getActivity())) {
                    if (rvLeagueTable.getVisibility() == View.GONE) {
                        tableHeader.setVisibility(View.GONE);
                        rvLeagueTable.setVisibility(View.GONE);
                        noDataView.setVisibility(View.GONE);
                        noInternetView.setVisibility(View.GONE);
                        loading.show();
                    }
                    new GetLeagueTable().execute(athEngine.LEAGUE_TABLE_BY_LEAGUE_ID_URL);
                }
                else {
                    loading.show();
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(1000);
                            }
                            catch (InterruptedException e) {
                                Log.i(athEngine.ATH_TAG, "TableFragment thread Exception: " + e.toString());
                            }
                            finally {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeToRefresh.setRefreshing(false);
                                        loading.hide();
                                        noInternetView.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }
                    };
                    timer.start();
                    tableHeader.setVisibility(View.GONE);
                    rvLeagueTable.setVisibility(View.GONE);
                    noDataView.setVisibility(View.GONE);
                    noInternetView.setVisibility(View.GONE);
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvLeagueTable.setLayoutManager(mLayoutManager);
        rvLeagueTable.setItemAnimator(new DefaultItemAnimator());
        rvLeagueTable.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
    }

    private void prepareRecycleView(LeagueTable mLeagueTable) {
        standings = new ArrayList<>();

        leagueName.setText(mLeagueTable.getLeagueCaption());
        standings = mLeagueTable.getStanding();

        adapter = new CustomListAdapter();
        rvLeagueTable.setAdapter(adapter);

        tableHeader.setVisibility(View.VISIBLE);
        rvLeagueTable.setVisibility(View.VISIBLE);
        swipeToRefresh.setRefreshing(false);
        if (loading.isShown()) {
            loading.hide();
        }
    }

    private class GetLeagueTable extends AsyncTask<String, String, String> {
        private LeagueTable mLeagueTable;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream source = athEngine.retrieveStream(uri[0]);
                Gson gson = new Gson();
                if (source != null) {
                    Reader reader = new InputStreamReader(source);
                    mLeagueTable = gson.fromJson(reader, LeagueTable.class);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                mLeagueTable = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mLeagueTable != null) {
                athEngine.setLeagueTable(mLeagueTable);
                leagueTable = mLeagueTable;
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);

                prepareRecycleView(mLeagueTable);
            }
            else {
                tableHeader.setVisibility(View.GONE);
                rvLeagueTable.setVisibility(View.GONE);
                loading.hide();
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.ViewLeagueTableHolder> {
        class ViewLeagueTableHolder extends RecyclerView.ViewHolder {
            LinearLayout row;
            TextView tvTablePosition;
            TextView tvTeamName;
            TextView tvTotalPlayed;
            TextView tvGoalDifference;
            TextView tvTotalPoints;

            ViewLeagueTableHolder(View view) {
                super(view);
                row = (LinearLayout) view.findViewById(R.id.singleItemLeagueTable);
                tvTablePosition = (TextView) view.findViewById(R.id.singleTablePosition);
                tvTeamName = (TextView) view.findViewById(R.id.singleTeamName);
                tvTotalPlayed = (TextView) view.findViewById(R.id.singleTotalPlayed);
                tvGoalDifference = (TextView) view.findViewById(R.id.singleGoalDifference);
                tvTotalPoints = (TextView) view.findViewById(R.id.singleTotalPoints);
            }
        }

        @Override
        public ViewLeagueTableHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_league_table, parent, false);
            return new ViewLeagueTableHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewLeagueTableHolder holder, final int position) {
            Standing standing = standings.get(position);
            if (athEngine.ATH_TEAM.equalsIgnoreCase(standing.getTeamName())) {
                Typeface selectedTypeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/SourceSansPro-Semibold.otf");
                holder.tvTablePosition.setTypeface(selectedTypeFace);
                holder.tvTablePosition.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
                holder.tvTeamName.setTypeface(selectedTypeFace);
                holder.tvTeamName.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
                holder.tvTotalPlayed.setTypeface(selectedTypeFace);
                holder.tvTotalPlayed.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
                holder.tvGoalDifference.setTypeface(selectedTypeFace);
                holder.tvGoalDifference.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
                holder.tvTotalPoints.setTypeface(selectedTypeFace);
                holder.tvTotalPoints.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
                if (athTeamStanding == null) {
                    athTeamStanding = standing;
                }
            }
            else {
                Typeface selectedTypeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/SourceSansPro-Regular.otf");
                holder.tvTablePosition.setTypeface(selectedTypeFace);
                holder.tvTablePosition.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
                holder.tvTeamName.setTypeface(selectedTypeFace);
                holder.tvTeamName.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
                holder.tvTotalPlayed.setTypeface(selectedTypeFace);
                holder.tvTotalPlayed.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
                holder.tvGoalDifference.setTypeface(selectedTypeFace);
                holder.tvGoalDifference.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
                holder.tvTotalPoints.setTypeface(selectedTypeFace);
                holder.tvTotalPoints.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
            }

            holder.tvTablePosition.setText(String.valueOf(standing.getPosition()));
            holder.tvTeamName.setText(athEngine.getShortTeamName(standing.getTeamName()));
            holder.tvTotalPlayed.setText(String.valueOf(standing.getPlayedGames()));
            holder.tvGoalDifference.setText(String.valueOf(standing.getGoalDifference()));
            holder.tvTotalPoints.setText(String.valueOf(standing.getPoints()));

            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((position+1) == athTeamStanding.getPosition()) {
                        BottomSheetDialogFragment bottomSheetDialogFragment = new AThTeamStatsBottomSheetDialogFragment();
                        bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            if(standings != null && standings.size() != 0) {
                return standings.size();
            }
            return 0;
        }
    }

    public static class AThTeamStatsBottomSheetDialogFragment extends BottomSheetDialogFragment {

        private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        dismiss();
                        break;
                }
            }


            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        };

        @Override
        public void setupDialog(Dialog dialog, int style) {
            super.setupDialog(dialog, style);
            View contentView = View.inflate(getContext(), R.layout.fragment_ath_team_stats, null);
            dialog.setContentView(contentView);

            if (athTeamStanding != null) {
                TextView totalPlayedValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsTotalPlayedValue);
                totalPlayedValue.setText(String.valueOf(athTeamStanding.getPlayedGames()));
                TextView pointsValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsPointsValue);
                pointsValue.setText(String.valueOf(athTeamStanding.getPoints()));

                TextView totalWinsValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsTotalWinsValue);
                totalWinsValue.setText(String.valueOf(athTeamStanding.getWins()));
                TextView totalDrawsValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsTotalDrawsValue);
                totalDrawsValue.setText(String.valueOf(athTeamStanding.getDraws()));
                TextView totalLosesValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsTotalLosesValue);
                totalLosesValue.setText(String.valueOf(athTeamStanding.getLosses()));

                TextView totalGoalsScoredValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsTotalGoalsScoredValue);
                totalGoalsScoredValue.setText(String.valueOf(athTeamStanding.getGoals()));
                TextView totalGoalsAgainstValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsTotalGoalsAgainstValue);
                totalGoalsAgainstValue.setText(String.valueOf(athTeamStanding.getGoalsAgainst()));
                TextView totalGoalsDiferenceValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsTotalGoalsDiferenceValue);
                totalGoalsDiferenceValue.setText(String.valueOf(athTeamStanding.getGoalDifference()));

                TextView homeWinsValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsHomeWinsValue);
                homeWinsValue.setText(String.valueOf(athTeamStanding.getHome().getWins()));
                TextView homeDrawsValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsHomeDrawsValue);
                homeDrawsValue.setText(String.valueOf(athTeamStanding.getHome().getDraws()));
                TextView homeLosesValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsHomeLosesValue);
                homeLosesValue.setText(String.valueOf(athTeamStanding.getHome().getLosses()));

                TextView homeGoalsScoredValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsHomeGoalsScoredValue);
                homeGoalsScoredValue.setText(String.valueOf(athTeamStanding.getHome().getGoals()));
                TextView homeGoalsAgainstValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsHomeGoalsAgainstValue);
                homeGoalsAgainstValue.setText(String.valueOf(athTeamStanding.getHome().getGoalsAgainst()));

                TextView awayWinsValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsAwayWinsValue);
                awayWinsValue.setText(String.valueOf(athTeamStanding.getAway().getWins()));
                TextView awayDrawsValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsAwayDrawsValue);
                awayDrawsValue.setText(String.valueOf(athTeamStanding.getAway().getDraws()));
                TextView awayLosesValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsAwayLosesValue);
                awayLosesValue.setText(String.valueOf(athTeamStanding.getAway().getLosses()));

                TextView awayGoalsScoredValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsAwayGoalsScoredValue);
                awayGoalsScoredValue.setText(String.valueOf(athTeamStanding.getAway().getGoals()));
                TextView awayGoalsAgainstValue = (TextView) contentView.findViewById(R.id.tvAThTeamStatsAwayGoalsAgainstValue);
                awayGoalsAgainstValue.setText(String.valueOf(athTeamStanding.getAway().getGoalsAgainst()));
            }

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();

            if( behavior != null && behavior instanceof BottomSheetBehavior ) {
                ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
                ((BottomSheetBehavior) behavior).setPeekHeight((int) getResources().getDimension(R.dimen.bottom_sheet_peek_height_ath_team_stats));
            }
        }
    }
}
