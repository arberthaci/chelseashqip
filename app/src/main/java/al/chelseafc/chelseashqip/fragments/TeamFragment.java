package al.chelseafc.chelseashqip.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.athMainActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import engine.DividerItemDecoration;
import engine.athEngine;
import models.Player;
import models.Players;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TeamFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeamFragment extends Fragment implements View.OnClickListener {
    private RelativeLayout tabTeamButton;
    private ImageView clubLogo;
    private View tabTeamButtonLine;
    private RelativeLayout tabPlayersButton;
    private View tabPlayersButtonLine;
    private LinearLayout tabTeam;
    private RelativeLayout tabPlayers;
    private AVLoadingIndicatorView loading;
    LinearLayout noInternetView;
    LinearLayout noDataView;
    private SwipeRefreshLayout swipeToRefresh;
    private Players players;
    private ArrayList<Player> allPlayers;
    private RecyclerView rvOfPlayers;
    private CustomListAdapter adapter;

    public TeamFragment() {}

    public static TeamFragment newInstance() {
        return new TeamFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        players = athEngine.getPlayers();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((athMainActivity) getActivity()).setFragmentTag(athEngine.TEAM_FRAGMENT_TAG, getResources().getString(R.string.menu_item_team));
        ((athMainActivity) getActivity()).closeKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_team, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();

        tabTeam.setVisibility(View.GONE);
        tabPlayers.setVisibility(View.VISIBLE);

        if (!athEngine.FIXTURES_BY_TEAM_ID_URL.equalsIgnoreCase("")) {
            athEngine.initializeTeamNames();
            if (players == null) {
                if (athEngine.isNetworkAvailable(getActivity())) {
                    new GetPlayers().execute(athEngine.PLAYERS_BY_TEAM_ID_URL);
                }
                else {
                    rvOfPlayers.setVisibility(View.GONE);
                    noDataView.setVisibility(View.GONE);
                    noInternetView.setVisibility(View.VISIBLE);
                }
            }
            else {
                if (players.getPlayers().size() == 0) {
                    rvOfPlayers.setVisibility(View.GONE);
                    loading.hide();
                    noInternetView.setVisibility(View.GONE);
                    noDataView.setVisibility(View.VISIBLE);
                }
                else {
                    prepareRecycleView(players);
                }
            }
        }
        else {
            rvOfPlayers.setVisibility(View.GONE);
            loading.hide();
            noInternetView.setVisibility(View.GONE);
            noDataView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tabTeamButton:
                tabTeamButtonLine.setVisibility(View.VISIBLE);
                tabPlayersButtonLine.setVisibility(View.GONE);
                tabTeam.setVisibility(View.VISIBLE);
                tabPlayers.setVisibility(View.GONE);
                break;
            case R.id.tabPlayersButton:
                tabTeamButtonLine.setVisibility(View.GONE);
                tabPlayersButtonLine.setVisibility(View.VISIBLE);
                tabTeam.setVisibility(View.GONE);
                tabPlayers.setVisibility(View.VISIBLE);
                if (players != null && players.getPlayers().size() != 0 && loading.isShown()) {
                    loading.hide();
                }
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        tabTeamButton = (RelativeLayout) view.findViewById(R.id.tabTeamButton);
        clubLogo = (ImageView) view.findViewById(R.id.ivChelseaLogo);
        tabTeamButtonLine = (View) view.findViewById(R.id.tabTeamButtonLine);
        tabPlayersButton = (RelativeLayout) view.findViewById(R.id.tabPlayersButton);
        tabPlayersButtonLine = (View) view.findViewById(R.id.tabPlayersButtonLine);
        tabTeam = (LinearLayout) view.findViewById(R.id.tabTeam);
        tabPlayers = (RelativeLayout) view.findViewById(R.id.tabPlayers);
        noDataView = (LinearLayout) view.findViewById(R.id.llNoDataNews);
        noInternetView = (LinearLayout) view.findViewById(R.id.llNoInternetNews);
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_to_refresh);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.loadingTeam);
        rvOfPlayers = (RecyclerView) view.findViewById(R.id.listOfPlayers);
    }

    private void afterInitialization() {
        tabTeamButtonLine.setVisibility(View.GONE);
        tabPlayersButtonLine.setVisibility(View.VISIBLE);

        noDataView.setVisibility(View.GONE);
        noInternetView.setVisibility(View.GONE);

        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (athEngine.isNetworkAvailable(getActivity())) {
                    if (rvOfPlayers.getVisibility() == View.GONE) {
                        rvOfPlayers.setVisibility(View.GONE);
                        noDataView.setVisibility(View.GONE);
                        noInternetView.setVisibility(View.GONE);
                        loading.show();
                    }
                    new GetPlayers().execute(athEngine.PLAYERS_BY_TEAM_ID_URL);
                }
                else {
                    loading.show();
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(1000);
                            }
                            catch (InterruptedException e) {
                                Log.i(athEngine.ATH_TAG, "TableFragment thread Exception: " + e.toString());
                            }
                            finally {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeToRefresh.setRefreshing(false);
                                        loading.hide();
                                        noInternetView.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }
                    };
                    timer.start();
                    rvOfPlayers.setVisibility(View.GONE);
                    noDataView.setVisibility(View.GONE);
                    noInternetView.setVisibility(View.GONE);
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvOfPlayers.setLayoutManager(mLayoutManager);
        rvOfPlayers.setItemAnimator(new DefaultItemAnimator());
        rvOfPlayers.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        Picasso.with(getActivity())
                .load(athEngine.TEAM_CREST_URL)
                .resize(500, 500)
                .placeholder(R.drawable.loading_image)
                .error(R.drawable.loading_image)
                .into(clubLogo);

        tabTeamButton.setOnClickListener(this);
        tabPlayersButton.setOnClickListener(this);
    }

    private void prepareRecycleView(Players mPlayer) {
        allPlayers = new ArrayList<>();
        allPlayers = mPlayer.getPlayers();

        Collections.sort(allPlayers, new Comparator<Player>() {
            @Override
            public int compare(Player player2, Player player1) {
                return  player2.getName().compareTo(player1.getName());
            }
        });

        adapter = new CustomListAdapter();
        rvOfPlayers.setAdapter(adapter);

        rvOfPlayers.setVisibility(View.VISIBLE);
        swipeToRefresh.setRefreshing(false);
        if (loading.isShown()) {
            loading.hide();
        }
    }

    private class GetPlayers extends AsyncTask<String, String, String> {
        private Players mPlayers;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream source = athEngine.retrieveStream(uri[0]);
                Gson gson = new Gson();
                if (source != null) {
                    Reader reader = new InputStreamReader(source);
                    mPlayers = gson.fromJson(reader, Players.class);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                mPlayers = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (mPlayers != null) {
                athEngine.setPlayers(mPlayers);
                players = mPlayers;

                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);

                prepareRecycleView(mPlayers);
            }
            else {
                rvOfPlayers.setVisibility(View.GONE);
                loading.hide();
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.ViewTeamHolder> {
        class ViewTeamHolder extends RecyclerView.ViewHolder {
            TextView tvPlayerName;
            TextView tvShirtNumber;
            TextView tvPosition;
            TextView tvBirthday;
            TextView tvNationality;
            TextView tvContractUntil;
            TextView tvMarketValue;
            CircleImageView ivPlayerImage;

            ViewTeamHolder(View view) {
                super(view);
                tvPlayerName = (TextView) view.findViewById(R.id.singlePlayerName);
                tvShirtNumber = (TextView) view.findViewById(R.id.singleShirtNumber);
                tvPosition = (TextView) view.findViewById(R.id.singlePosition);
                tvBirthday = (TextView) view.findViewById(R.id.singleBirthday);
                tvNationality = (TextView) view.findViewById(R.id.singleNationality);
                tvContractUntil = (TextView) view.findViewById(R.id.singleContractUntil);
                tvMarketValue = (TextView) view.findViewById(R.id.singleMarketValue);
                ivPlayerImage = (CircleImageView) view.findViewById(R.id.singlePlayerImage);
            }
        }

        @Override
        public ViewTeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_players_team, parent, false);
            return new ViewTeamHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewTeamHolder holder, int position) {
            Player player = allPlayers.get(position);

            holder.tvPlayerName.setText(player.getName());
            if (String.valueOf(player.getJerseyNumber()).equalsIgnoreCase("0")) {
                holder.tvShirtNumber.setText("??");
            }
            else {
                holder.tvShirtNumber.setText(String.valueOf(player.getJerseyNumber()));
            }
            holder.tvPosition.setText(athEngine.formatPlayerPosition(player.getPosition()));
            holder.tvBirthday.setText(athEngine.getFormattedDate(player.getDateOfBirth(), athEngine.TIME_TYPE_DATE_ONLY));
            holder.tvNationality.setText(player.getNationality());
            holder.tvContractUntil.setText(athEngine.getFormattedDate(player.getContractUntil(), athEngine.TIME_TYPE_DATE_ONLY));
            holder.tvMarketValue.setText(player.getMarketValue());

            Picasso.with(getActivity())
                    .load(athEngine.THUMBNAIL_WEBSITE_URL + player.getName().toLowerCase().replaceAll("\\s","") + ".png")
                    .placeholder(R.drawable.loading_image)
                    .error(R.drawable.loading_image)
                    .into(holder.ivPlayerImage);
        }

        @Override
        public int getItemCount() {
            if(allPlayers != null && allPlayers.size() != 0) {
                return allPlayers.size();
            }
            return 0;
        }
    }
}
