package al.chelseafc.chelseashqip.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.athMainActivity;
import engine.DividerItemDecoration;
import engine.athEngine;
import models.Fixture;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LastTenMeetingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LastTenMeetingsFragment extends Fragment {
    private RecyclerView rvOfResults;
    private ArrayList<Fixture> results;
    LinearLayout noDataView;

    public LastTenMeetingsFragment() {}

    public static LastTenMeetingsFragment newInstance() {
        return new LastTenMeetingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((athMainActivity) getActivity()).setFragmentTag(athEngine.LAST_TEN_MEETINGS_TAG, getResources().getString(R.string.activity_last_ten_meetings));
        ((athMainActivity) getActivity()).closeKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_last_ten_meetings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();

        if (results.size() == 0) {
            rvOfResults.setVisibility(View.GONE);
            noDataView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        rvOfResults = (RecyclerView) view.findViewById(R.id.listOfLastTenMeetings);
        noDataView = (LinearLayout) view.findViewById(R.id.llNoDataNews);
    }

    private void afterInitialization() {
        noDataView.setVisibility(View.GONE);

        RecyclerView.LayoutManager mLayoutManagerOfResults = new LinearLayoutManager(getActivity().getApplicationContext());
        rvOfResults.setLayoutManager(mLayoutManagerOfResults);
        rvOfResults.setItemAnimator(new DefaultItemAnimator());
        rvOfResults.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        results = athEngine.getFixtureInfo().getHead2head().getFixtures();
        CustomListAdapter resultsAdapter = new CustomListAdapter();
        rvOfResults.setAdapter(resultsAdapter);
    }

    private class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.ViewFixturesAndResultsHolder> {

        class ViewFixturesAndResultsHolder extends RecyclerView.ViewHolder {
            LinearLayout row;
            TextView tvCompetition;
            TextView tvHomeTeam;
            TextView tvResult;
            TextView tvAwayTeam;
            TextView tvDate;

            ViewFixturesAndResultsHolder(View view) {
                super(view);
                row = (LinearLayout) view.findViewById(R.id.rowFixturesAndResults);
                tvCompetition = (TextView) view.findViewById(R.id.singleCompetition);
                tvHomeTeam = (TextView) view.findViewById(R.id.singleHomeTeam);
                tvResult = (TextView) view.findViewById(R.id.singleResult);
                tvAwayTeam = (TextView) view.findViewById(R.id.singleAwayTeam);
                tvDate = (TextView) view.findViewById(R.id.singleDate);
            }
        }

        @Override
        public CustomListAdapter.ViewFixturesAndResultsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_fixtures_and_results, parent, false);
            return new CustomListAdapter.ViewFixturesAndResultsHolder(itemView);
        }

        @Override
        public void onBindViewHolder(CustomListAdapter.ViewFixturesAndResultsHolder holder, final int position) {
            Fixture result = results.get(position);

            holder.tvHomeTeam.setText(athEngine.getShortTeamName(result.getHomeTeamName()));
            holder.tvResult.setText(result.getResult().getGoalsHomeTeam() + " - " + result.getResult().getGoalsAwayTeam());
            holder.tvAwayTeam.setText(athEngine.getShortTeamName(result.getAwayTeamName()));
            holder.tvDate.setText(athEngine.getFormattedDate(result.getDate(), athEngine.TIME_TYPE_DATETIME));

            if (result.getHomeTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                holder.tvHomeTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            }
            else {
                holder.tvHomeTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
            }
            if (result.getAwayTeamName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                holder.tvAwayTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            }
            else {
                holder.tvAwayTeam.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondarySecond));
            }

            if (!athEngine.getShortTeamName(result.getHomeTeamName()).equalsIgnoreCase(result.getHomeTeamName()) && !athEngine.getShortTeamName(result.getAwayTeamName()).equalsIgnoreCase(result.getAwayTeamName())) {
                holder.tvCompetition.setText(athEngine.ATH_LEAGUE_NAME);
            }
            else {
                holder.tvCompetition.setText(athEngine.ATH_EUROPEAN_LEAGUE_NAME);
            }
        }

        @Override
        public int getItemCount() {
            int count = 0;
            if (results != null && results.size() != 0) {
                count = results.size();
            }
            return count;
        }
    }
}
