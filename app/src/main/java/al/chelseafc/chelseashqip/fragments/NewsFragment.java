package al.chelseafc.chelseashqip.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.activities.athMainActivity;
import engine.athEngine;
import models.Article;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFragment extends Fragment {
    private AVLoadingIndicatorView loading;
    LinearLayout noInternetView;
    LinearLayout noDataView;
    private SwipeRefreshLayout swipeToRefresh;
    private RecyclerView rvNews;
    private CustomListAdapter adapter;
    private ArrayList<Article> articles;

    public NewsFragment() {}

    public static NewsFragment newInstance() {
        return new NewsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        articles = athEngine.getArticles();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((athMainActivity) getActivity()).setFragmentTag(athEngine.NEWS_FRAGMENT_TAG, getResources().getString(R.string.menu_item_news));
        ((athMainActivity) getActivity()).closeKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();

        if (articles == null) {
            if (athEngine.isNetworkAvailable(getActivity())) {
                new GetAllNews().execute(athEngine.NEWS_WEBSITE_URL);
            }
            else {
                rvNews.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                noInternetView.setVisibility(View.VISIBLE);
            }
        }
        else {
            if (articles.size() == 0) {
                rvNews.setVisibility(View.GONE);
                loading.hide();
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.VISIBLE);
            }
            else {
                prepareRecycleView();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.loadingHome);
        noDataView = (LinearLayout) view.findViewById(R.id.llNoDataNews);
        noInternetView = (LinearLayout) view.findViewById(R.id.llNoInternetNews);
        swipeToRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_to_refresh);
        rvNews = (RecyclerView) view.findViewById(R.id.rvNews);
    }

    private void afterInitialization() {
        rvNews.setVisibility(View.GONE);
        noDataView.setVisibility(View.GONE);
        noInternetView.setVisibility(View.GONE);
        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimarySecond));
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (athEngine.isNetworkAvailable(getActivity())) {
                    if (rvNews.getVisibility() == View.GONE) {
                        rvNews.setVisibility(View.GONE);
                        noDataView.setVisibility(View.GONE);
                        noInternetView.setVisibility(View.GONE);
                        loading.show();
                    }
                    new GetAllNews().execute(athEngine.NEWS_WEBSITE_URL);
                }
                else {
                    loading.show();
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(1000);
                            }
                            catch (InterruptedException e) {
                                Log.i(athEngine.ATH_TAG, "NewsFragment thread Exception: " + e.toString());
                            }
                            finally {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeToRefresh.setRefreshing(false);
                                        loading.hide();
                                        noInternetView.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }
                    };
                    timer.start();
                    rvNews.setVisibility(View.GONE);
                    noDataView.setVisibility(View.GONE);
                    noInternetView.setVisibility(View.GONE);
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvNews.setLayoutManager(mLayoutManager);
        rvNews.setItemAnimator(new DefaultItemAnimator());
    }

    private void scrapWebSite(String mUrl) {
        try {
            ArrayList<Article> mArticles = new ArrayList<>();
            Document document = Jsoup.connect(mUrl).get();

            Elements jSoupArticles = document.getElementById("tc-recent-posts-2").getElementsByTag("li");
            for (Element jSoupArticle : jSoupArticles) {
                String title;
                try {
                    title = jSoupArticle.getElementsByClass("post-data").get(0).getElementsByTag("a").get(0).text();
                }
                catch (Exception e) {
                    title = "-1";
                    Log.i(athEngine.ATH_TAG, "NewsFragment scrapWebSite title Exception: " + e.toString());
                }
                String body;
                try {
                    body = jSoupArticle.getElementsByClass("post-data").get(0).getElementsByTag("p").get(0).text();
                }
                catch (Exception e) {
                    body = "";
                    Log.i(athEngine.ATH_TAG, "NewsFragment scrapWebSite body Exception: " + e.toString());
                }
                String link;
                try {
                    link = jSoupArticle.getElementsByClass("post-data").get(0).getElementsByTag("a").get(0).attr("href");
                }
                catch (Exception e) {
                    link = "-1";
                    Log.i(athEngine.ATH_TAG, "NewsFragment scrapWebSite link Exception: " + e.toString());
                }
                String imageUrl;
                try {
                    imageUrl = jSoupArticle.getElementsByClass("post-img").get(0).getElementsByTag("a").get(0).getElementsByTag("img").get(0).attr("src");
                    String tempImagePostfix = imageUrl.substring(imageUrl.lastIndexOf("."));
                    String tempImagePrefix = imageUrl.substring(0, (imageUrl.length() - tempImagePostfix.length() - 7));
                    imageUrl = tempImagePrefix + tempImagePostfix;
                }
                catch (Exception e) {
                    imageUrl = "-1";
                    Log.i(athEngine.ATH_TAG, "NewsFragment scrapWebSite imageUrl Exception: " + e.toString());
                }
                String date;
                try {
                    String[] tempDate = jSoupArticle.getElementsByClass("post-data").get(0).getElementsByClass("recent-post-meta").get(0).text().split(",")[0].split(" ");
                    date = tempDate[1] + " " + athEngine.getAlbanianMonthName(tempDate[0]);
                }
                catch (Exception e) {
                    date = "-1";
                    Log.i(athEngine.ATH_TAG, "NewsFragment scrapWebSite date Exception: " + e.toString());
                }

                if (!title.equalsIgnoreCase("-1")) {
                    Article tempArticle = new Article();
                    tempArticle.setTitle(title);
                    tempArticle.setBody(body);
                    tempArticle.setDate(date.toUpperCase());
                    tempArticle.setImageUrl(imageUrl);
                    tempArticle.setLink(link);
                    mArticles.add(tempArticle);
                }
            }
            if (mArticles.size() >= 2 && mArticles.get(0).getLink().equalsIgnoreCase(mArticles.get(1).getLink())) {
                mArticles.remove(0);
            }
            articles = mArticles;
        }
        catch (Exception e) {
            Log.i(athEngine.ATH_TAG, "NewsFragment scrapWebSite Exception: " + e.toString());
        }
    }

    private void prepareRecycleView() {
        rvNews.setVisibility(View.VISIBLE);
        adapter = new CustomListAdapter();
        rvNews.setAdapter(adapter);
        loading.hide();
        swipeToRefresh.setRefreshing(false);
    }

    private class GetAllNews extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (articles == null) {
                articles = new ArrayList<>();
            }
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                scrapWebSite(uri[0]);
            }
            catch (Exception e) {
                Log.i(athEngine.ATH_TAG, "NewsFragment GetAllNews doInBackground Exception: " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            athEngine.setArticles(articles);
            if (articles.size() == 0) {
                rvNews.setVisibility(View.GONE);
                loading.hide();
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.VISIBLE);
            }
            else {
                noInternetView.setVisibility(View.GONE);
                noDataView.setVisibility(View.GONE);
                prepareRecycleView();
            }
        }
    }

    private class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.ViewTeamHolder> {
        class ViewTeamHolder extends RecyclerView.ViewHolder {
            LinearLayout row;
            TextView tvTitle;
            TextView tvBody;
            TextView tvDate;
            ImageView ivArticleImage;

            ViewTeamHolder(View view) {
                super(view);
                row = (LinearLayout) view.findViewById(R.id.singleNews);
                tvTitle = (TextView) view.findViewById(R.id.articleTitle);
                tvBody = (TextView) view.findViewById(R.id.articleBody);
                tvDate = (TextView) view.findViewById(R.id.articleDate);
                ivArticleImage = (ImageView) view.findViewById(R.id.articleImage);
            }
        }

        @Override
        public ViewTeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_news, parent, false);
            return new ViewTeamHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewTeamHolder holder, int position) {
            final Article article = articles.get(position);

            holder.tvTitle.setText(article.getTitle());
            holder.tvBody.setText(article.getBody());
            if (article.getDate().equalsIgnoreCase("-1")) {
                holder.tvDate.setVisibility(View.GONE);
            }
            else {
                holder.tvDate.setText(article.getDate());
            }
            Picasso.with(getActivity())
                    .load(article.getImageUrl())
                    .placeholder(R.drawable.loading_image)
                    .error(R.drawable.loading_image)
                    .into(holder.ivArticleImage);

            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (article.getLink().equalsIgnoreCase("-1")) {
                        // do nothing
                    }
                    else {
                        ((athMainActivity) getActivity()).openLink(getActivity(), article.getLink());
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            if(articles != null && articles.size() != 0) {
                return articles.size();
            }
            return 0;
        }
    }
}
