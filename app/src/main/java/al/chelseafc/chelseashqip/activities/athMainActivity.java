package al.chelseafc.chelseashqip.activities;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.SubMenu;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import al.chelseafc.chelseashqip.R;
import al.chelseafc.chelseashqip.fragments.AboutUsFragment;
import al.chelseafc.chelseashqip.fragments.FeedbackFragment;
import al.chelseafc.chelseashqip.fragments.FixturesAndResultsFragment;
import al.chelseafc.chelseashqip.fragments.NewsFragment;
import al.chelseafc.chelseashqip.fragments.TableFragment;
import al.chelseafc.chelseashqip.fragments.TeamFragment;
import engine.CustomTabHelper;
import engine.CustomTypefaceSpan;
import engine.athEngine;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class athMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String currentFragmentTag = "empty";
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private Menu menu;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private CustomTabHelper mCustomTabHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ath_main);

        initializeComponents();
        afterInitialization();

        if (savedInstanceState == null) {
            openNewsFragment();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer!= null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            if (currentFragmentTag.equalsIgnoreCase(athEngine.LAST_TEN_MEETINGS_TAG)) {
                closeFragmentByTag(currentFragmentTag);
                currentFragmentTag = athEngine.FIXTURES_AND_RESULTS_FRAGMENT_TAG;
                toolbar.setTitle(getResources().getString(R.string.menu_item_fixtures_and_results));
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.ath_fragment_container, FixturesAndResultsFragment.newInstance(), currentFragmentTag)
                        .commit();
            }
            else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ath_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                if (!currentFragmentTag.equalsIgnoreCase(athEngine.NEWS_FRAGMENT_TAG)) {
                    closeFragmentByTag(currentFragmentTag);
                    openNewsFragment();
                    menu.getItem(0).setChecked(true);
                }
                break;
            case R.id.nav_fixtures_and_results:
                if (!currentFragmentTag.equalsIgnoreCase(athEngine.FIXTURES_AND_RESULTS_FRAGMENT_TAG)) {
                    closeFragmentByTag(currentFragmentTag);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.ath_fragment_container, FixturesAndResultsFragment.newInstance(), currentFragmentTag)
                            .commit();
                    menu.getItem(1).setChecked(true);
                }
                break;
            case R.id.nav_table:
                if (!currentFragmentTag.equalsIgnoreCase(athEngine.TABLE_FRAGMENT_TAG)) {
                    closeFragmentByTag(currentFragmentTag);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.ath_fragment_container, TableFragment.newInstance(), currentFragmentTag)
                            .commit();
                    menu.getItem(2).setChecked(true);
                }
                break;
            case R.id.nav_team:
                if (!currentFragmentTag.equalsIgnoreCase(athEngine.TEAM_FRAGMENT_TAG)) {
                    closeFragmentByTag(currentFragmentTag);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.ath_fragment_container, TeamFragment.newInstance(), currentFragmentTag)
                            .commit();
                    menu.getItem(3).setChecked(true);
                }
                break;
            case R.id.nav_visit_website:
                openLink(athMainActivity.this, athEngine.APP_WEBSITE_URL);
                break;
            case R.id.nav_about_us:
                if (!currentFragmentTag.equalsIgnoreCase(athEngine.ABOUT_US_FRAGMENT_TAG)) {
                    closeFragmentByTag(currentFragmentTag);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.ath_fragment_container, AboutUsFragment.newInstance(), currentFragmentTag)
                            .commit();
                    unCheckMenuItems();
                }
                break;
            case R.id.nav_feedback:
                if (!currentFragmentTag.equalsIgnoreCase(athEngine.FEEDBACK_FRAGMENT_TAG)) {
                    closeFragmentByTag(currentFragmentTag);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.ath_fragment_container, FeedbackFragment.newInstance(), currentFragmentTag)
                            .commit();
                    unCheckMenuItems();
                }
                break;
            case R.id.nav_exit_application:
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initializeComponents() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.ath_coordinator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mCustomTabHelper = new CustomTabHelper(athMainActivity.this);
    }


    private void afterInitialization() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(athMainActivity.this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        menu = navigationView.getMenu();
        prepareMenuItemFonts();
        menu.getItem(0).setChecked(true);
        MenuItem tools = menu.findItem(R.id.itemGroupLabelCommunicateWithUs);
        SpannableString s = new SpannableString(tools.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.AthItemGroupStle), 0, s.length(), 0);
        tools.setTitle(s);
        navigationView.setNavigationItemSelectedListener(this);
        mCustomTabHelper.prepareUrl(athEngine.APP_WEBSITE_URL);
    }

    private void prepareMenuItemFonts() {
        for (int i=0; i<menu.size(); i++) {
            MenuItem mMenuItem = menu.getItem(i);

            SubMenu subMenu = mMenuItem.getSubMenu();
            if (subMenu!=null && subMenu.size()>0) {
                for (int j=0; j<subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            applyFontToMenuItem(mMenuItem);
        }
    }

    private void applyFontToMenuItem(MenuItem mMenuItem) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/SourceSansPro-Semibold.otf");
        SpannableString mNewTitle = new SpannableString(mMenuItem.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mMenuItem.setTitle(mNewTitle);
    }

    private void unCheckMenuItems() {
        for (int i=0; i<4; i++) {
            menu.getItem(i).setChecked(false);
        }
    }

    private void openNewsFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .add(R.id.ath_fragment_container, NewsFragment.newInstance(), currentFragmentTag)
                .commit();
    }

    private void closeFragmentByTag(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager.beginTransaction().remove(fragment).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
        }
    }

    public void showMessageOnSnack(String message) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).setAction("Mbyll", null).show();
    }

    public void openLink(Context context, String url) {
        CustomTabHelper.CustomTabsUiBuilder builder = new CustomTabHelper.CustomTabsUiBuilder();
        builder.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        builder.setShowTitle(true);
        builder.setCloseButtonIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.abc_ic_ab_back_material));
        builder.setStartAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        builder.setExitAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        try {
            mCustomTabHelper.openUrl(url, builder);
        }
        catch (Exception e) {
            Log.i(athEngine.ATH_TAG, "athMainActivity openLink Exception: " + e.toString());
        }
    }

    public void setFragmentTag(String fragmentTag, String toolbarTitle) {
        currentFragmentTag = fragmentTag;
        toolbar.setTitle(toolbarTitle);
    }

    public void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
