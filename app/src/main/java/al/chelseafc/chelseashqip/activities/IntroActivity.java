package al.chelseafc.chelseashqip.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import al.chelseafc.chelseashqip.R;
import engine.athCustomService;
import engine.athEngine;
import models.League;
import models.Team;
import models.TeamsOfLeague;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by root on 16-03-21.
 */
public class IntroActivity extends AppCompatActivity {

    private CoordinatorLayout clIntro;
    private Calendar calendar;
    private SharedPreferences prefs;
    private boolean canGoBack = true;
    private GcmNetworkManager mGcmNetworkManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_intro);

        clIntro = (CoordinatorLayout) findViewById(R.id.cl_intro_activity);

        openApplication();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
        if (canGoBack) {
            super.onBackPressed();
        }
    }

    private void setReminder() {
        Calendar mxCalendar = Calendar.getInstance();
        int hourOfDay = mxCalendar.get(Calendar.HOUR_OF_DAY);
        long seconds = 0;

        if (hourOfDay < 4) {
            int hoursLeft = (4 - hourOfDay);
            int minutesLeft = (60 - (mxCalendar.get(Calendar.MINUTE)) % 60);
            if (minutesLeft != 0) {
                hoursLeft--;
            }
            seconds = (hoursLeft * 60 * 60) + (minutesLeft  * 60);
        }
        else if (hourOfDay < 10) {
            int hoursLeft = (10 - hourOfDay);
            int minutesLeft = (60 - (mxCalendar.get(Calendar.MINUTE)) % 60);
            if (minutesLeft != 0) {
                hoursLeft--;
            }
            seconds = (hoursLeft * 60 * 60) + (minutesLeft  * 60);
        }
        else if (hourOfDay < 16) {
            int hoursLeft = (16 - hourOfDay);
            int minutesLeft = (60 - (mxCalendar.get(Calendar.MINUTE)) % 60);
            if (minutesLeft != 0) {
                hoursLeft--;
            }
            seconds = (hoursLeft * 60 * 60) + (minutesLeft  * 60);
        }
        else if (hourOfDay < 22) {
            int hoursLeft = (22 - hourOfDay);
            int minutesLeft = (60 - (mxCalendar.get(Calendar.MINUTE)) % 60);
            if (minutesLeft != 0) {
                hoursLeft--;
            }
            seconds = (hoursLeft * 60 * 60) + (minutesLeft  * 60) ;
        }
        else {
            int hoursLeft = (24 - hourOfDay + 4);
            int minutesLeft = (60 - (mxCalendar.get(Calendar.MINUTE)) % 60);
            if (minutesLeft != 0) {
                hoursLeft--;
            }
            seconds = (hoursLeft * 60 * 60) + (minutesLeft  * 60) ;
        }

        Task task = new OneoffTask.Builder()
                .setService(athCustomService.class)
                .setExecutionWindow(seconds, seconds + 30)
                .setTag(athEngine.ATH_ONE_OFF_GCM_TAG)
                .setUpdateCurrent(true)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setRequiresCharging(false)
                .build();

        mGcmNetworkManager.schedule(task);
    }

    private class GetAllLeagues extends AsyncTask<String, String, String> {
        ArrayList<League> leagues;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream source = athEngine.retrieveStream(uri[0]);
                Gson gson = new Gson();
                if (source != null) {
                    Reader reader = new InputStreamReader(source);
                    Type collectionType = new TypeToken<Collection<League>>(){}.getType();
                    leagues = gson.fromJson(reader, collectionType);
                }
            }
            catch (Exception e) {
                Log.i(athEngine.ATH_TAG, "IntroActivity GetAllLeagues doInBackground Exception: " + e.toString());
                leagues = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (leagues != null) {
                for (League leagueIterator : leagues) {
                    if (leagueIterator.getLeague().equalsIgnoreCase(athEngine.ATH_LEAGUE_ID)) {
                        athEngine.setLeagueCompetitionIdUrl(leagueIterator.get_links().getSelf().getHref());
                        athEngine.setAllTeamsByLeagueIdUrl(leagueIterator.get_links().getTeams().getHref());
                        athEngine.setLeagueTableByLeagueIdUrl(leagueIterator.get_links().getLeagueTable().getHref());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt("actual_week", calendar.get(Calendar.WEEK_OF_MONTH));
                        editor.putString("LEAGUE_COMPETITION_ID_URL", athEngine.getLeagueCompetitionIdUrl());
                        editor.putString("ALL_TEAMS_BY_LEAGUE_ID_URL", athEngine.ALL_TEAMS_BY_LEAGUE_ID_URL);
                        editor.putString("LEAGUE_TABLE_BY_LEAGUE_ID_URL", athEngine.LEAGUE_TABLE_BY_LEAGUE_ID_URL);
                        editor.apply();
                    }
                    else if (leagueIterator.getLeague().equalsIgnoreCase(athEngine.ATH_EUROPEAN_LEAGUE_ID)) {
                        athEngine.setEuropeanLeagueCompetitionIdUrl(leagueIterator.get_links().getSelf().getHref());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("EUROPEAN_LEAGUE_COMPETITION_ID_URL", athEngine.getEuropeanLeagueCompetitionIdUrl());
                        editor.apply();
                    }
                }
                if (!athEngine.ALL_TEAMS_BY_LEAGUE_ID_URL.equalsIgnoreCase("")) {
                    new GetAllTeamsByLeagueId().execute(athEngine.ALL_TEAMS_BY_LEAGUE_ID_URL);
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.ath_engine_no_data_error_message, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), R.string.ath_engine_no_data_error_message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetAllTeamsByLeagueId extends AsyncTask<String, String, String> {
        TeamsOfLeague teamsOfLeague;
        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream source = athEngine.retrieveStream(uri[0]);
                Gson gson = new Gson();
                Reader reader = new InputStreamReader(source);
                teamsOfLeague = gson.fromJson(reader, TeamsOfLeague.class);
            }
            catch (Exception e) {
                Log.i(athEngine.ATH_TAG, "IntroActivity GetAllTeamsByLeagueId doInBackground Exception: " + e.toString());
                teamsOfLeague = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (teamsOfLeague != null) {
                for (Team teamIterator : teamsOfLeague.getTeams()) {
                    if (teamIterator.getName().equalsIgnoreCase(athEngine.ATH_TEAM)) {
                        athEngine.setTeamInfoByTeamIdUrl(teamIterator.get_links().getSelf().getHref());
                        athEngine.setFixturesByTeamIdUrl(teamIterator.get_links().getFixtures().getHref());
                        athEngine.setPlayersByTeamIdUrl(teamIterator.get_links().getPlayers().getHref());
                        athEngine.setTeamCrestUrl(teamIterator.getCrestUrl());
                        athEngine.setTeamMarketValue(teamIterator.getSquadMarketValue());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("TEAM_INFO_BY_TEAM_ID_URL", athEngine.TEAM_INFO_BY_TEAM_ID_URL);
                        editor.putString("FIXTURES_BY_TEAM_ID_URL", athEngine.FIXTURES_BY_TEAM_ID_URL);
                        editor.putString("PLAYERS_BY_TEAM_ID_URL", athEngine.PLAYERS_BY_TEAM_ID_URL);
                        editor.putString("TEAM_MARKET_VALUE", athEngine.TEAM_MARKET_VALUE);
                        editor.commit();
                    }
                }
            }
            else {
                Toast.makeText(getApplicationContext(), R.string.ath_engine_no_data_error_message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetApplicationStats extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... uri) {
            try {
                scrapWebSite(uri[0]);
            }
            catch (Exception e) {
                Log.i(athEngine.ATH_TAG, "IntroActivity GetApplicationStats doInBackground Exception: " + e.toString());
            }
            return null;
        }
    }

    private void scrapWebSite(String mUrl) {
        try {
            Document document = Jsoup.connect(mUrl).get();

            Element jSoupStats = document.getElementById("android-stats");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("STATS_INSTALLING", jSoupStats.text().split("/")[0]);
            editor.putString("STATS_RATING", jSoupStats.text().split("/")[1]);
            editor.putInt("today", calendar.get(Calendar.DATE));
            editor.commit();
        }
        catch (Exception e) {
            Log.i(athEngine.ATH_TAG, "IntroActivity scrapWebSite Exception: " + e.toString());
        }
    }

    private void connectToAPI() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(2500);
                }
                catch (InterruptedException e) {
                    Log.i(athEngine.ATH_TAG, "IntroActivity thread Exception: " + e.toString());
                }
                finally {
                    Intent intent = new Intent(getApplicationContext(), athMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    getApplicationContext().startActivity(intent);
                }
            }
        };
        timer.start();

        prefs = getApplicationContext().getSharedPreferences("SharedPreferencesAThEngine", MODE_PRIVATE);
        calendar = Calendar.getInstance();
        if ((int) calendar.get(Calendar.WEEK_OF_MONTH) == prefs.getInt("actual_week", -1)) {
            athEngine.setLeagueCompetitionIdUrl(prefs.getString("LEAGUE_COMPETITION_ID_URL", ""));
            athEngine.setEuropeanLeagueCompetitionIdUrl(prefs.getString("EUROPEAN_LEAGUE_COMPETITION_ID_URL", ""));
            athEngine.setAllTeamsByLeagueIdUrl(prefs.getString("ALL_TEAMS_BY_LEAGUE_ID_URL", ""));
            athEngine.setLeagueTableByLeagueIdUrl(prefs.getString("LEAGUE_TABLE_BY_LEAGUE_ID_URL", ""));
            athEngine.setTeamInfoByTeamIdUrl(prefs.getString("TEAM_INFO_BY_TEAM_ID_URL", ""));
            athEngine.setFixturesByTeamIdUrl(prefs.getString("FIXTURES_BY_TEAM_ID_URL", ""));
            athEngine.setPlayersByTeamIdUrl(prefs.getString("PLAYERS_BY_TEAM_ID_URL", ""));
            athEngine.setTeamMarketValue(prefs.getString("TEAM_MARKET_VALUE", ""));
        }
        else {
            new GetAllLeagues().execute(athEngine.ALL_LEAGUES_URL);
        }
        if ((int) calendar.get(Calendar.DATE) != prefs.getInt("today", -1)) {
            new GetApplicationStats().execute(athEngine.NEWS_WEBSITE_URL);
            mGcmNetworkManager = GcmNetworkManager.getInstance(this);
            setReminder();
        }
        athEngine.initializeTeamNames();
    }

    private void openApplication() {
        if (athEngine.isNetworkAvailable(IntroActivity.this)) {
            canGoBack = false;
            connectToAPI();
        }
        else {
            Snackbar
                    .make(clIntro, R.string.ath_engine_snackbar_no_internet_error_message, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ath_engine_snackbar_no_internet_retry_action, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openApplication();
                        }
                    })
                    .setDuration(Snackbar.LENGTH_INDEFINITE)
                    .show();
        }
    }
}
